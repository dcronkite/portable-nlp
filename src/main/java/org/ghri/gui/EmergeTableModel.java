/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * EmergeTableModel.java 0.2 2013-10-18
 */
package org.ghri.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ghri.emerge.DataItem;
import org.ghri.emerge.MetaColumn;
import org.ghri.util.DataArrayList;

import javax.swing.table.DefaultTableModel;
import java.util.List;

/**
 * TableModel for the TablePanel, requires use of DataItems (ie, MatchData, DataEntry)
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
class EmergeTableModel<T extends DataItem> extends DefaultTableModel {

    private final Logger logger = LogManager.getLogger(EmergeTableModel.class);
    List<MetaColumn> columns;
    private DataArrayList<T> data;


    public EmergeTableModel() {
        data = new DataArrayList<>();
    }

    public void setData(DataArrayList<T> data, boolean replaceData) {
        if (data != null)
            if (replaceData)
                this.data = data;
            else
                this.data.addAll(data);
    }

    public void updateOffset(int offset) {
        data.updateOffset(offset);
    }

    public void clearData() {
        this.data.clear();
    }

    @Override
    public int getRowCount() {
        if (data != null) {
            return data.size();
        }
        return 0;
    }

    @Override
    public int getColumnCount() {
        if (columns == null)
            return 0;

        int columnCount = 0;
        for (MetaColumn column : columns) {
            if (column.isVisible()) {
                columnCount += 1;
            }
        }
        return columnCount;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        int newIdx = getIndexOfUsedColumn(columnIndex);

        DataItem di = data.get(rowIndex);
        return di.get(newIdx);
    }


    @Override
    public String getColumnName(int columnIndex) {
        int newIdx = getIndexOfUsedColumn(columnIndex);
        return columns.get(newIdx).getName();
    }

    public void setColumns(List<MetaColumn> columns2) {
        this.columns = columns2;
    }

    public int getIndexOfUsedColumn(int columnIndex) {
        int count = -1;
        for (int i = 0; i < columns.size(); i++) {
            if (columns.get(i).isVisible()) {
                count += 1;
            }
            if (count == columnIndex)
                return i;
        }
        logger.error(String.format("Requested column \"%d\" is greater than number of visible columns (%d).", columnIndex + 1, count + 1));
        return -1; // this should never happen
    }

    public void removeRow(int row) {
        if (data != null)
            if (row < columns.size())
                data.remove(row);
    }

    public void hideColumn(int columnIndex) {
        logger.info("Hiding Column: " + columnIndex);
        if (columns != null)
            columns.get(columnIndex).setVisible(false);
    }

    public void renameColumn(int columnIndex, String newName) {
        columns.get(columnIndex).setName(newName);
    }
}
