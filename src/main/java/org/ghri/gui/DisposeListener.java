/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DisposeListener.java 0.2 2013-10-18
 */
package org.ghri.gui;

/**
 * Listener added to frames which will not close. This will
 * let the calling process close them.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
interface DisposeListener {

    void dispose();
}
