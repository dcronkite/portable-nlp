/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * ToolbarListener.java 0.2 2013-10-18
 */
package org.ghri.gui.toolbar;

/**
 * Listens for button clicks on Toolbar
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @see Toolbar
 * @see ToolbarEnum
 * @since 0.2 2013-10-18
 */
public interface ToolbarListener {

    void clicked(ToolbarEnum te);

}
