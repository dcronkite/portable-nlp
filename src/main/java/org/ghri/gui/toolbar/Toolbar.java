/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * Toolbar.java 0.3 2013-10-18
 */
package org.ghri.gui.toolbar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * GUI button Toolbar in main application
 *
 * @author David Cronkite
 * @version 0.3 2013-10-18
 * @see ToolbarEnum
 * @see ToolbarListener
 * @since 0.2 2013-10-18
 */
public class Toolbar extends JToolBar implements ActionListener {

    private final JButton processBtn;
    private final JButton clearDataBtn;
    private final JButton clearDocsBtn;
    private final JButton debugBtn;
    private ToolbarListener listener;


    public Toolbar() {
        processBtn = new JButton("Process Text");
        clearDataBtn = new JButton("Clear Results");
        clearDocsBtn = new JButton("Clear Documents");
        debugBtn = new JButton("Debug Regex");

        Dimension d = clearDocsBtn.getPreferredSize();
        processBtn.setPreferredSize(d);
        clearDataBtn.setPreferredSize(d);
        debugBtn.setPreferredSize(d);


        // remove border to make draggable
//		setBorder(BorderFactory.createEtchedBorder());
//		setLayout(new FlowLayout(FlowLayout.LEFT));

        processBtn.addActionListener(this);
        clearDataBtn.addActionListener(this);
        clearDocsBtn.addActionListener(this);
        debugBtn.addActionListener(this);

        add(processBtn);
        add(clearDataBtn);
        add(clearDocsBtn);
        add(debugBtn);
    }


    public void setToolbarListener(ToolbarListener listener) {
        this.listener = listener;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clicked = (JButton) e.getSource();

        if (clicked == processBtn) {
            if (listener != null) {
                listener.clicked(ToolbarEnum.PROCESS);
            }
        } else if (clicked == clearDataBtn) {
            if (listener != null) {
                listener.clicked(ToolbarEnum.CLEAR_DATA);
            }
        } else if (clicked == clearDocsBtn) {
            if (listener != null) {
                listener.clicked(ToolbarEnum.CLEAR_DOCUMENTS);
            }
        } else if (clicked == debugBtn) {
            if (listener != null) {
                listener.clicked(ToolbarEnum.DEBUG);
            }
        }
    }
}
