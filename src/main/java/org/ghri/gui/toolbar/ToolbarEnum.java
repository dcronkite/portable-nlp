/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * ToolbarEnum.java 0.3 2013-10-18
 */
package org.ghri.gui.toolbar;

/**
 * Enum containing different messages from Toolbar
 * button clicks.
 *
 * @author David Cronkite
 * @version 0.3 2013-10-18
 * @see Toolbar
 * @see ToolbarListener
 * @since 0.2 2013-10-18
 */
public enum ToolbarEnum {
    PROCESS,
    CLEAR_DATA,
    CLEAR_DOCUMENTS, //added v0.3
    DEBUG  // added v0.5.0
}
