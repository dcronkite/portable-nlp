/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.gui;

import javax.swing.*;
import java.awt.*;

/**
 * @author David
 * @version 0.1 2016-05-28
 */
public class DialogLayout {

    public static void layoutComponents(JDialog dialog, JButton okBtn, JButton cancelBtn, JComponent field) {
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));


        buttonsPanel.add(okBtn);
        okBtn.setPreferredSize(cancelBtn.getPreferredSize());
        buttonsPanel.add(cancelBtn);


        dialog.setLayout(new BorderLayout());
        dialog.add(field, BorderLayout.CENTER);
        dialog.add(buttonsPanel, BorderLayout.SOUTH);
    }
}
