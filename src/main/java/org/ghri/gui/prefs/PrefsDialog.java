/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * PrefsDialog.java 0.2 2013-10-18
 */
package org.ghri.gui.prefs;

import javax.swing.*;
import java.awt.*;

/**
 * Dialog for changing database configurations.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class PrefsDialog extends JDialog {

    private final JButton okBtn;
    private final JButton cancelBtn;
    private final JTextField connectionStringField;
    private final JTextField userField;
    private final JPasswordField passField;
    private final JCheckBox rememberBtn;

    private PrefsListener prefsListener;

    public PrefsDialog(JFrame parent) {
        super(parent, "Preferences", false);

        okBtn = new JButton("OK");
        cancelBtn = new JButton("Cancel");
        rememberBtn = new JCheckBox("Remember preferences?");

        userField = new JTextField(15);
        passField = new JPasswordField(15);
        connectionStringField = new JTextField(15);

        layoutComponents();

        okBtn.addActionListener(e -> {
            String user = userField.getText();
            char[] password = passField.getPassword();
            String connectionString = connectionStringField.getText();

            setVisible(false);

            /*
             * set selected preferences
             */
            if (prefsListener != null) {
                if (rememberBtn.isSelected()) {
                    prefsListener.setPreferences(user, new String(password), connectionString, true);
                } else {
                    prefsListener.setPreferences(user, new String(password), connectionString, false);
                    resetFields();
                }
            }
        });

        cancelBtn.addActionListener(e -> {
            setVisible(false);
            resetFields();
        });
        setDimAndLocation(parent);
    }

    private void setDimAndLocation(Component parent) {
        setSize(350, 250);
        setLocationRelativeTo(parent);
    }

    @Override
    public void setVisible(boolean b) {
        if (b)
            setDimAndLocation(getParent());
        super.setVisible(b);
    }

    private void resetFields() {
        userField.setText("");
        passField.setText("");
        connectionStringField.setText("");
    }

    private void layoutComponents() {

        JPanel controlsPanel = new JPanel();
        int space = 10;
        controlsPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(space, space, space, space),
                BorderFactory.createTitledBorder("Database Configurations")));

        JPanel buttonsPanel = new JPanel();

        controlsPanel.setLayout(new GridBagLayout());
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        GridBagConstraints gc = new GridBagConstraints();

		/*
		 * Initial config
		 */
        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;
        Insets rightPadding = new Insets(0, 0, 0, 20);
        Insets leftPadding = new Insets(0, 20, 0, 0);
        Insets noPadding = new Insets(0, 0, 0, 0);
		
		/*
		 * First row
		 * Username 
		 */
        gc.gridx = 0;
        gc.gridy++;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        controlsPanel.add(new JLabel("Username:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        controlsPanel.add(userField, gc);
		
		
		/*
		 * Second row
		 * Password 
		 */
        gc.gridx = 0;
        gc.gridy++;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        controlsPanel.add(new JLabel("Password:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        controlsPanel.add(passField, gc);
		
		
		
		/*
		 * row 3
		 */

        gc.gridx = 0;
        gc.gridy++;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        controlsPanel.add(new JLabel("Connection URL: "), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        controlsPanel.add(connectionStringField, gc);
		
		/*
		 * row 4
		 * Save Settings
		 */
        gc.gridx = 0;
        gc.gridy++;
        gc.gridwidth = 2;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = leftPadding;
        controlsPanel.add(rememberBtn, gc);
		
		/*
		 *  Buttons Panel
	    */
        Dimension btnSize = cancelBtn.getPreferredSize();
        okBtn.setPreferredSize(btnSize);
        buttonsPanel.add(okBtn);
        buttonsPanel.add(cancelBtn);


        setLayout(new BorderLayout());
        add(controlsPanel, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
    }

    public void setDefaults(String user, String password, String connectionString, boolean select) {
        userField.setText(user);
        passField.setText(password);
        connectionStringField.setText(connectionString);
        rememberBtn.setSelected(select);
    }

    public void setPrefsListener(PrefsListener prefsListener) {
        this.prefsListener = prefsListener;
    }
}
