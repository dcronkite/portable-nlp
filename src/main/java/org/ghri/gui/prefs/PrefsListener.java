/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * PrefsListener.java 0.2 2013-10-18
 */
package org.ghri.gui.prefs;

/**
 * Listener for updating database configurations.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public interface PrefsListener {

    void setPreferences(String username, String password,
                        String connectionString, boolean selected);
}
