/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * TablePanel.java 0.2 2013-10-18
 */
package org.ghri.gui;

import org.ghri.emerge.DataItem;
import org.ghri.emerge.MetaColumn;
import org.ghri.util.DataArrayList;

import javax.swing.*;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * Shows rows/columns of DataItems.
 * Appears in each tab in MainFrame
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
class TablePanel<T extends DataItem> extends JPanel {

    private final JTable table;
    private final EmergeTableModel<T> tableModel;
    private final TableColumnAdjuster tca;
    private final TableColumnModel columnModel;

    private final JPopupMenu popupMenu;
    private final JPopupMenu headerPopupMenu;

    private RenameTextField renameTextField;

    private TablePanelListener listener;

    public TablePanel() {
        tableModel = new EmergeTableModel<>();
        columnModel = new DefaultTableColumnModel();
        table = new JTable(tableModel, columnModel);

        popupMenu = new JPopupMenu();
        headerPopupMenu = new JPopupMenu();


        setLayout(new BorderLayout());

        setPopupMenu();
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);

                int row = table.rowAtPoint(e.getPoint());
                table.getSelectionModel().setSelectionInterval(row, row);
                columnModel.getSelectionModel().setSelectionInterval(row, row);
                if (e.getButton() == MouseEvent.BUTTON3) {
                    popupMenu.show(table, e.getX(), e.getY());
                }
            }
        });

        table.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int idx = table.convertColumnIndexToModel(table.columnAtPoint(e.getPoint()));
                if (idx >= 0) {
                    table.setColumnSelectionInterval(idx, idx);
                    if (e.getButton() == MouseEvent.BUTTON3)
                        headerPopupMenu.show(table.getTableHeader(), e.getX(), e.getY());
                }
            }
        });

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setFillsViewportHeight(true);
//		table.createDefaultColumnsFromModel();
        table.setAutoCreateColumnsFromModel(true);
        tca = new TableColumnAdjuster(table);
        add(new JScrollPane(table), BorderLayout.CENTER);
//		add(table, BorderLayout.CENTER);
        adjustAndRepaint();
    }


    public void setData(DataArrayList<T> data, boolean replaceData) {
        tableModel.setData(data, replaceData);
        adjustAndRepaint();
    }

    public void clearData() {
        tableModel.clearData();
        adjustAndRepaint();
    }

    private void adjustAndRepaint() {
        tca.adjustColumns();
        table.revalidate();
        table.repaint();
        table.revalidate();
        table.getParent().revalidate();
    }


    public void setColumns(List<MetaColumn> columns) {
        tableModel.setColumns(columns);
        updateColumns(columns);
    }


    public void updateColumns(List<MetaColumn> columns) {
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            columnModel.removeColumn(columnModel.getColumn(i));
        }
        columns.stream().filter(MetaColumn::isVisible).forEach(triple -> tableModel.addColumn(triple.getColumn()));
        adjustAndRepaint();
    }

    public void updateOffset(int offset) {
        tableModel.updateOffset(offset);
    }

    private void setPopupMenu() {
        JMenuItem removeItem = new JMenuItem("Delete Row");
        removeItem.addActionListener(e -> {
            int row = table.getSelectedRow();
            tableModel.removeRow(row);
            tableModel.fireTableRowsDeleted(row, row);
            if (listener != null) {
                adjustAndRepaint();
                listener.rowDeleted(row);
            }
        });

        popupMenu.add(removeItem);


        JMenuItem renameColumn = new JMenuItem("Rename");
        renameColumn.addActionListener(e -> {
            int col = table.getSelectedColumn();
            final int columnIndex = tableModel.getIndexOfUsedColumn(col);
            renameTextField = new RenameTextField(tableModel.columns.get(columnIndex).getName());
            renameTextField.setTextListener(text -> listener.columnRenamed(columnIndex, text));
            renameTextField.pack();
            renameTextField.setVisible(true);
//				listener.columnRenamed(columnIndex, "NewName");
        });
        JMenuItem hideColumn = new JMenuItem("Hide");
        hideColumn.addActionListener(e -> {
            int col = table.getSelectedColumn();
            int columnIndex = tableModel.getIndexOfUsedColumn(col);
            listener.columnDeleted(columnIndex);
        });
        headerPopupMenu.add(renameColumn);
        headerPopupMenu.add(hideColumn);
    }


    public void setTablePanelListener(TablePanelListener tablePanelListener) {
        listener = tablePanelListener;
    }
}