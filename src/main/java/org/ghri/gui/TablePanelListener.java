/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * TablePanelListener.java 0.2 2013-10-18
 */
package org.ghri.gui;

/**
 * Listens to events on TablePanel
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @see TablePanel
 * @since 0.2 2013-10-18
 */
interface TablePanelListener {

    @SuppressWarnings("EmptyMethod")
    void rowDeleted(int row);

    void columnDeleted(int columnIndex);

    void columnRenamed(int columnIndex, String string);
}
