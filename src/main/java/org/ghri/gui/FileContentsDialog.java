/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * AboutPanel.java 0.3 2013-10-18
 */
package org.ghri.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Displays contents of file or files as a dialog.
 *
 * @author David Cronkite
 * @version 0.3 2013-10-18
 * @since 0.3 2013-10-18
 */
class FileContentsDialog extends JDialog {

    private static final Logger logger = LogManager.getLogger(FileContentsDialog.class);

    private final JTextArea textArea;

    public FileContentsDialog(Window parent, String title, String documentHeading) {
        super(parent, title, ModalityType.APPLICATION_MODAL);
        textArea = new JTextArea();

        add(new JScrollPane(textArea));
        textArea.append("* * * * * * * * * * * * * * * * * * * * * * * *");
        textArea.append("* * * * * * * * * * * * * * * * * * * * * * *\n");
        textArea.append("                         ");
        textArea.append(documentHeading + "\n\n");
        textArea.append("* * * * * * * * * * * * * * * * * * * * * * * *");
        textArea.append("* * * * * * * * * * * * * * * * * * * * * * *\n");
    }

    void loadFromStream(InputStream is) {
        BufferedReader br = null;
        textArea.append("* * * * * * * * * * * * * * * * * * * * * * * *");
        textArea.append("* * * * * * * * * * * * * * * * * * * * * * *\n");
        try {
            br = new BufferedReader(new InputStreamReader(is));
            String text;
            while ((text = br.readLine()) != null) {
                textArea.append(text + "\n");
            }
        } catch (IOException e) {
            logger.error(e);
            textArea.append("Failed to load.");
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                //ignore
            }
        }
    }

    private void setDimAndLocation(Component parent) {
        setSize(450, 550);
        setLocationRelativeTo(parent);
    }

    @Override
    public void setVisible(boolean b) {
        if (b)
            setDimAndLocation(getParent());
        super.setVisible(b);
    }
}
