/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DelimiterFileChooser.java 0.2 2013-10-18
 */
package org.ghri.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * SaveFileChooser with option to specify how columns
 * should be delimited.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
class DelimiterFileChooser extends JFileChooser {

    private final JFileChooser fileChooser;

    private final JRadioButton textBtn;
    private final JTextField textField;

    private final JRadioButton documentsBtn;
    private final JRadioButton resultDataBtn;
    private final JRadioButton resultMatchesBtn;
    private final JRadioButton allMatchesBtn;
    private final JRadioButton tabBtn;
    private final JFrame frame;
    private final ActionListener[] innerListeners;
    private JButton saveBtn;
    private JButton cancelBtn;
    private String delimiter;
    private int index;
    private int exitValue;
    private DisposeListener disposeListener;


    DelimiterFileChooser() {
        super();
        super.setName("Save File Dialog");
        frame = new JFrame("Save File Dialog");
        exitValue = JFileChooser.CANCEL_OPTION;
        fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        setControlButtonsAreShown(false);
        frame.add(fileChooser);

        delimiter = "\t";

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        JPanel lowerPanel = new JPanel();
        lowerPanel.setLayout(new BorderLayout());

        ButtonGroup panelButtons = new ButtonGroup();
        ButtonGroup contentPanelButtons = new ButtonGroup();

        textField = new JTextField(5);
        textField.setText("||");
        textField.setEnabled(false);

        tabBtn = new JRadioButton("Tab", true);
        JPanel tabPanel = new JPanel();
        tabPanel.add(tabBtn);
        tabPanel.setLayout(new FlowLayout());

        textBtn = new JRadioButton("", false);
        textBtn.addChangeListener(e -> {
            boolean isText = textBtn.isSelected();
            textField.setEnabled(isText);
            if (isText) {
                textField.setForeground(Color.BLACK);
            } else {
                textField.setForeground(Color.GRAY);
            }
        });


        documentsBtn = new JRadioButton("Documents");
        resultDataBtn = new JRadioButton("Result Data");
        resultDataBtn.setSelected(true);
        index = 1;
        resultMatchesBtn = new JRadioButton("Result Matches");
        allMatchesBtn = new JRadioButton("All Matches");

        // radio groups
        panelButtons.add(tabBtn);
        panelButtons.add(textBtn);

        contentPanelButtons.add(documentsBtn);
        contentPanelButtons.add(resultDataBtn);
        contentPanelButtons.add(resultMatchesBtn);
        contentPanelButtons.add(allMatchesBtn);

        JPanel otherOption = new JPanel();
        otherOption.setLayout(new FlowLayout());
        otherOption.add(textBtn);
        otherOption.add(textField);

        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new FlowLayout());
        radioPanel.add(new JLabel(" Select Delimiter:"), FlowLayout.LEFT);
        radioPanel.add(tabPanel);
        radioPanel.add(otherOption);

        JPanel contentRadioPanel = new JPanel();
        contentRadioPanel.setLayout(new FlowLayout());
        contentRadioPanel.add(new JLabel(" Tab to Save:"), FlowLayout.LEFT);
        contentRadioPanel.add(documentsBtn);
        contentRadioPanel.add(resultDataBtn);
        contentRadioPanel.add(resultMatchesBtn);
        contentRadioPanel.add(allMatchesBtn);


        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        saveBtn = null;
        cancelBtn = null;
        setSaveAndCancelButtons(fileChooser);
        innerListeners = saveBtn.getActionListeners();
        saveBtn.addActionListener(e -> {
            for (ActionListener al : innerListeners) {
                al.actionPerformed(e);
            }
            if (tabBtn.isSelected()) {
                delimiter = "\t";
            } else if (textBtn.isSelected()) {
                delimiter = textField.getText();
            }
            if (documentsBtn.isSelected())
                index = 0;
            else if (resultDataBtn.isSelected())
                index = 1;
            else if (resultMatchesBtn.isSelected())
                index = 2;
            else if (allMatchesBtn.isSelected())
                index = 3;

            setSelectedFile(fileChooser.getSelectedFile());
            if (getSelectedFile() != null)
                exitValue = JFileChooser.APPROVE_OPTION;
            else
                exitValue = JFileChooser.CANCEL_OPTION;
            frame.dispose();
            disposeListener.dispose();
        });

        cancelBtn.addActionListener(e -> {
            setSelectedFile(null);
            exitValue = JFileChooser.CANCEL_OPTION;
            disposeListener.dispose();
        });

        buttonsPanel.add(saveBtn);
        buttonsPanel.add(cancelBtn);

        panel.add(radioPanel, BorderLayout.WEST);
        panel.add(buttonsPanel, BorderLayout.EAST);

        lowerPanel.add(contentRadioPanel, BorderLayout.WEST);
        lowerPanel.add(panel, BorderLayout.SOUTH);


        setLayout(new BorderLayout());
        add(fileChooser, BorderLayout.CENTER);
        add(lowerPanel, BorderLayout.SOUTH);
    }

    private void setSaveAndCancelButtons(Container cont) {
        for (int i = 0; i < cont.getComponentCount(); i++) {
            Component c = cont.getComponent(i);
            if (c instanceof JButton) {
                JButton btn = (JButton) c;

                if (btn.getText() != null) {
                    if (btn.getText().equals("Save")) {
                        saveBtn = btn;
                    } else if (btn.getText().equals("Cancel"))
                        cancelBtn = btn;
                }
            } else if (c instanceof Container) {
                setSaveAndCancelButtons((Container) c);
            }
        }
    }

    String getDelimiter() {
        return delimiter;
    }

    int getTabIndex() {
        return index;
    }

    void addDisposeListener(DisposeListener disposeListener) {
        this.disposeListener = disposeListener;
    }

    int getExitValue() {
        return exitValue;
    }
}
