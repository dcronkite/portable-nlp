/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;


class RenameTextField extends JDialog {

    private final JTextField textField;
    private MyTextListener listener;

    public RenameTextField() {
        this("");
    }

    RenameTextField(String oldName) {
        super();
        super.setTitle("Rename Column");
        textField = new JTextField(oldName);

        listener = null;

        JButton okBtn = new JButton("OK");
        okBtn.setMnemonic(KeyEvent.VK_C);
        okBtn.addActionListener(e -> {
            listener.setText(textField.getText().trim());
            dispose();
        });

        JButton cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(e -> dispose());

        DialogLayout.layoutComponents(this, okBtn, cancelBtn, textField);
        setDimAndLocation(getParent());
    }

    private void setDimAndLocation(Component parent) {
        setSize(179, 96);
        setLocation(MouseInfo.getPointerInfo().getLocation());
    }

    public void setVisible(boolean b) {
        if (b)
            setDimAndLocation(getParent());
        super.setVisible(b);
    }

    void setTextListener(MyTextListener listener) {
        this.listener = listener;
    }
}
