/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * MainFrame.java 0.3 2013-10-18
 */
package org.ghri.gui;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ghri.emerge.Controller;
import org.ghri.emerge.DataItem;
import org.ghri.gui.column.ColumnDialog;
import org.ghri.gui.column.ColumnEvent;
import org.ghri.gui.column.OutputDataColumnEvent;
import org.ghri.gui.db.ReadDatabaseDialog;
import org.ghri.gui.db.WriteDatabaseDialog;
import org.ghri.gui.prefs.PrefsDialog;
import org.ghri.gui.toolbar.Toolbar;
import org.ghri.gui.toolbar.ToolbarEnum;
import org.ghri.log.log4j2.plugins.LogPanelAppender;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.Dialog.ModalityType;
import java.awt.event.*;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.prefs.Preferences;

/**
 * Main window for project. Primary gui.
 *
 * @author David Cronkite
 * @version 0.3 2013-10-18
 * @since 0.2 2013-10-18
 */
public class MainFrame extends JFrame {

    private static final Logger logger = LogManager.getLogger(MainFrame.class);
    private static final String optionsFilename = "options.prop";
    private final JScrollPane logScrollPane;
    private final JFileChooser fileChooser;
    private final JFileChooser regexProcFileChooser;  // RegexProcessor file chooser
    private final DelimiterFileChooser saveFileChooser;
    private final DelimitedOpenFileChooser regexFileChooser;
    private final JDialog regexFileChooserFrame;
    private final JDialog saveFileChooserFrame;
    private final JTabbedPane tabPane;
    private final ColumnDialog columnDialog;
    private final ColumnDialog outputColumnDialog;
    private final PrefsDialog prefsDialog;
    private final ReadDatabaseDialog databaseDialog;
    private final WriteDatabaseDialog writeDatabaseDialog;
    private final JSplitPane splitPane;
    private final TablePanel<DataItem> resultMatchesTable;
    private final TablePanel<DataItem> matchesTable;
    private final TablePanel<DataItem> dataEntryTable;
    private final TablePanel<DataItem> resultDataTable;
    private final FileContentsDialog aboutPageDialog;
    private final FileContentsDialog tutorialPageDialog;

    private final Toolbar toolbar;

    private final Controller controller;

    private final Preferences prefs;

    private final StopWatch stopwatch;
    private List<TablePanel> tablePanelList;

    /**
     *
     */
    public MainFrame(Controller controller) {
        super("PortableNLP Application");
        LogPanel logPanel = new LogPanel();
        logScrollPane = new JScrollPane(logPanel);
        logScrollPane.setMinimumSize(new Dimension(150, 300));
        LogPanelAppender.setLogPanel(logPanel);

        this.controller = controller;

        resultMatchesTable = new TablePanel<>();
        resultDataTable = new TablePanel<>();
        matchesTable = new TablePanel<>();
        fileChooser = new JFileChooser();
        toolbar = new Toolbar();
        columnDialog = new ColumnDialog(this, "Columns");
        prefsDialog = new PrefsDialog(this);
        databaseDialog = new ReadDatabaseDialog(this);
        writeDatabaseDialog = new WriteDatabaseDialog(this);
        dataEntryTable = new TablePanel<>();


        regexProcFileChooser = new JFileChooser();
        regexProcFileChooser.setDialogTitle("Select Regular Expression Processor...");
        regexProcFileChooser.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory() || f.getName().toLowerCase().endsWith(".java") ||
                        f.getName().toLowerCase().endsWith(".class");
            }

            @Override
            public String getDescription() {
                return "Regex Processor File (*.java|*.class)";
            }
        });

        regexFileChooser = new DelimitedOpenFileChooser();
        regexFileChooserFrame = new JDialog(this, ModalityType.APPLICATION_MODAL);
        regexFileChooserFrame.add(regexFileChooser);
        regexFileChooserFrame.setMinimumSize(regexFileChooser.getPreferredSize());
        regexFileChooser.addDisposeListener(regexFileChooserFrame::dispose);

        saveFileChooser = new DelimiterFileChooser();
        saveFileChooserFrame = new JDialog(this, ModalityType.APPLICATION_MODAL);
        saveFileChooserFrame.add(saveFileChooser);
        saveFileChooserFrame.setMinimumSize(saveFileChooser.getPreferredSize());
        saveFileChooser.addDisposeListener(saveFileChooserFrame::dispose);


        // Restore fileChooser's directory to last used
        File optionsFile = new File(optionsFilename);
        File[] outputColumnDialogFiles = new File[5];
        if (optionsFile.exists()) {
            try {
                restoreOptions(fileChooser, regexFileChooser, regexProcFileChooser, outputColumnDialogFiles);
            } catch (IOException ioe) {
                logger.warn("Could not find options file");
                ioe.printStackTrace();
            }
        }


        prefs = Preferences.userRoot().node("connection");

        columnDialog.setColumnListener(events -> {
            for (ColumnEvent ce : events) {
                int tableIndex = ce.getTableIndex();
                int offset = controller.updateColumn(ce, tableIndex);
                tablePanelList.get(tableIndex).updateOffset(offset);
                tablePanelList.get(tableIndex).updateColumns(controller.getColumn(tableIndex));
            }
        });

        outputColumnDialog = new ColumnDialog(this, "Columns", outputColumnDialogFiles);
        outputColumnDialog.setColumnListener(events -> {
            int offset = 20;
            boolean debug = false;
            int size = events.size() / 2;
            List<File> files = new ArrayList<>();
            File[] dest = new File[size];
            String[] del = new String[size];

            for (ColumnEvent ce : events) {
                int tableIndex = ce.getTableIndex();
                if (tableIndex < 0) {  // summary page
                    OutputDataColumnEvent odce = (OutputDataColumnEvent) ce;
                    files = Arrays.asList(odce.getFiles());
                } else if (ce instanceof OutputDataColumnEvent) {
                    OutputDataColumnEvent odce = (OutputDataColumnEvent) ce;
                    dest[tableIndex] = odce.getFile();
                    del[tableIndex] = odce.getDelimiter();
                } else {
                    offset = controller.updateColumn(ce, tableIndex);
                    tablePanelList.get(tableIndex).updateOffset(offset);
                    tablePanelList.get(tableIndex).updateColumns(controller.getColumn(tableIndex));
                }
            }

            //noinspection ConstantConditions
            controller.runBatchMode(files, debug, dest, del, offset);
        });

        /**
         * each of these tables are displayed
         * as separate tabs
         */
        tablePanelList = new ArrayList<>();
        tablePanelList.add(dataEntryTable);
        tablePanelList.add(resultDataTable);
        tablePanelList.add(resultMatchesTable);
        tablePanelList.add(matchesTable);


        tabPane = new JTabbedPane();
        tabPane.addTab("Documents", null, dataEntryTable, "Loaded Documents");
        tabPane.addTab("Result Data", null, resultDataTable, "Result Data");
        tabPane.addTab("Result Matches", null, resultMatchesTable, "All Relevant Matches");
        tabPane.addTab("Matches", null, matchesTable, "All Matches");


        /**
         * add logging console
         */
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, logScrollPane, tabPane);
        splitPane.setDividerLocation(0.25);
        splitPane.setOneTouchExpandable(true);


        /**
         * add toolbar buttons
         */
        toolbar.setToolbarListener(te -> {
            if (te == ToolbarEnum.PROCESS)
                processText();
            else if (te == ToolbarEnum.CLEAR_DATA)
                clearData();
            else if (te == ToolbarEnum.CLEAR_DOCUMENTS)
                clearDocuments();
            else if (te == ToolbarEnum.DEBUG)
                debugText();
        });

		/*
         * Preferences have been modified.
		 */
        prefsDialog.setPrefsListener((username, password, connectionUrl, selected) -> {
            /*
             * save preferences if selected
             */
            if (selected) {
                prefs.put("user", username);
                prefs.put("password", password);
                prefs.put("connectionUrl", connectionUrl);
                //noinspection ConstantConditions
                prefs.putBoolean("selected", selected);
            } else {
                prefs.put("user", "");
                prefs.put("password", "");
                prefs.put("connectionUrl", "");
                //noinspection ConstantConditions
                prefs.putBoolean("selected", selected);
            }

            /*
             * configure connection
             */
            controller.configureConnection(username, password,
                    connectionUrl);
        });
        String user = prefs.get("user", System.getProperty("user.name", ""));
        if (user.trim().equals("")) user = System.getProperty("user.name", "");
        String password = prefs.get("password", "");
        String connectionUrl = prefs.get("connectionUrl", "");
        Boolean select = prefs.getBoolean("selected", false);

        prefsDialog.setDefaults(user, password, connectionUrl, select);
        if (select) {
            logger.info("Attempting to configure connection with saved preferences.");
            controller.configureConnection(user, password, connectionUrl);
        }
		
		/*
		 * Database Dialog Listener
		 */
        databaseDialog.setListener((tableName, nameColumn, subnameColumn, dateColumn, textColumn) -> {
            try {
                controller.addTableEntries(tableName, nameColumn, subnameColumn, dateColumn, textColumn);
                logger.info("Added document entries.");
            } catch (SQLException e) {
                logger.error("Failed to add document entries: ", e.toString());
                logger.error(e);
            }
        });

        writeDatabaseDialog.setListener(controller::writeToTable);

		
		/*
		 * 
		 * About Page Dialog
		 * 
		 */
        InputStream apacheLicenseTxt = MainFrame.class.getResourceAsStream("/about/apache-license.txt");
        InputStream acknowledgementsTxt = MainFrame.class.getResourceAsStream("/about/acknowledgements.txt");
        InputStream aboutTxt = MainFrame.class.getResourceAsStream("/about/about.txt");
        InputStream requirementsTxt = MainFrame.class.getResourceAsStream("/about/requirements.txt");
        aboutPageDialog = new FileContentsDialog(this, "About", "ABOUT PORTABLE NLP");
        aboutPageDialog.loadFromStream(aboutTxt);
        aboutPageDialog.loadFromStream(acknowledgementsTxt);
        aboutPageDialog.loadFromStream(requirementsTxt);
        aboutPageDialog.loadFromStream(apacheLicenseTxt);
		
		/*
		 * 
		 * Tutorial Page Dialog
		 * 
		 */
        InputStream tutorialTxt = MainFrame.class.getResourceAsStream("/tutorial/tutorials.txt");
        tutorialPageDialog = new FileContentsDialog(this, "Tutorials", "TUTORIALS");
        tutorialPageDialog.loadFromStream(tutorialTxt);

        stopwatch = new StopWatch();


        setJMenuBar(createMenuBar());

        layoutComponents(1);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    storeOptions(fileChooser, regexFileChooser, regexProcFileChooser, outputColumnDialog);
                } catch (Exception storeException) {
                    logger.warn("Could not store options");
                    storeException.printStackTrace();
                }
                controller.closeConnection();
                dispose();
                System.gc();
            }
        });

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new Dimension(500, 300));
        setSize(760, 500);
        setVisible(true);

        logger.info("Application Loaded");

        if (controller.notReady()) {
            if (regexProcFileChooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION) {
                File procFile = regexProcFileChooser.getSelectedFile();
                controller.setup(procFile);
            }
        }
        setupTableLayout();
        updateTable();
    }

    /**
     * stores options so they persist between runs. as of 7/16/14 just
     * storing last used directory in fileChooser
     *
     * @param fileChooser the fileChooser whose last directory we're storing
     */
    private static void storeOptions(JFileChooser fileChooser,
                                     DelimitedOpenFileChooser regexFileChooser,
                                     JFileChooser procFileChooser,
                                     ColumnDialog outputColumnDialog
    )
            throws IOException {
        File file = new File(optionsFilename);
        Properties p = new Properties();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));

        // document file chooser
        File currDir = fileChooser.getCurrentDirectory();
        p.setProperty("last_doc_dir", currDir.toString());

        // regex file chooser
        File cRxDir = regexFileChooser.getCurrentDirectory();
        p.setProperty("last_regex_dir", cRxDir.toString());

        // regex processor file chooser
        File cProcDir = procFileChooser.getCurrentDirectory();
        p.setProperty("last_proc_dir", cProcDir.toString());

        //
        File[] cOutDirs = outputColumnDialog.getCurrentOutputDirectories();
        p.setProperty("last_batch_doc_dir", cOutDirs[0].toString());
        String[] names = new String[]{null, "doc", "resdata", "resmatch", "matches"};
        for (int i = 1; i < cOutDirs.length; i++) {
            if (cOutDirs[i] != null) {
                p.setProperty(String.format("last_batch_out_dir_%s", names[i]), cOutDirs[i].toString());
            }
        }

        p.store(bw, "File chooser properties");
    }

    private static void restoreOptions(JFileChooser fileChooser,
                                       DelimitedOpenFileChooser regexFileChooser,
                                       JFileChooser procFileChooser,
                                       File[] outputColumnDialogFiles)
            throws IOException {
        File file = new File(optionsFilename);
        Properties p = new Properties();
        BufferedReader br = new BufferedReader(new FileReader(file));
        p.load(br);

        fileChooser.setCurrentDirectory(restoreOptionsFile(p, "last_doc_dir"));
        regexFileChooser.setCurrentDirectory(restoreOptionsFile(p, "last_regex_dir"));
        procFileChooser.setCurrentDirectory(restoreOptionsFile(p, "last_proc_dir"));
        outputColumnDialogFiles[0] = restoreOptionsFile(p, "last_batch_doc_dir");
        outputColumnDialogFiles[1] = restoreOptionsFile(p, "last_batch_out_dir_doc");
        outputColumnDialogFiles[2] = restoreOptionsFile(p, "last_batch_out_dir_resdata");
        outputColumnDialogFiles[3] = restoreOptionsFile(p, "last_batch_out_dir_resmatch");
        outputColumnDialogFiles[4] = restoreOptionsFile(p, "last_batch_out_dir_matches");
    }


    private static File restoreOptionsFile(Properties p, String name) {
        String ds = p.getProperty(name);
        if (ds != null) {
            return new File(ds);
        }
        return null;
    }


    /**
     * removes all result data
     * removes all matchdata entries currently shown in table
     */
    private void clearData() {
        resultMatchesTable.clearData();
        matchesTable.clearData();
        resultDataTable.clearData();
//		dataEntryTable.clearData();
    }

    /**
     * removes all loaded documents
     * removes documents currently shown in table
     */
    private void clearDocuments() {
        dataEntryTable.clearData();
    }

    private void updateTable() {
        resultMatchesTable.setData(controller.getResultMatches(), true);
        matchesTable.setData(controller.getAllMatches(), true);
        dataEntryTable.setData(controller.getDataEntries(), true);
        resultDataTable.setData(controller.getResultData(), true);
    }

    private void processText() {
        stopwatch.reset();
        stopwatch.start();
        boolean processed = controller.processData();
        stopwatch.stop();
        if (processed) {
            updateTable();
            logger.info("Processing took " + stopwatch.getNanoTime() / 1000000 + "ms.");
        }
    }

    private void debugText() {
        stopwatch.reset();
        stopwatch.start();
        boolean processed = controller.processData(true);
        stopwatch.stop();
        if (processed) {
            updateTable();
            logger.info("Debugging took " + stopwatch.getNanoTime() / 1000000 + "ms.");
        }
    }


    private void setupTableLayout() {
        for (int i = 0; i < tablePanelList.size(); i++) {
            final int i_final = i;
            TablePanel table = tablePanelList.get(i);
            table.setTablePanelListener(new TablePanelListener() {
                @Override
                public void rowDeleted(int row) {

                }

                @Override
                public void columnDeleted(int columnIndex) {
                    // update mainframe list of columns
                    controller.setColumnBoolean(i_final, columnIndex, false);
                    TablePanel tp = (TablePanel) tabPane.getComponentAt(i_final);
                    tp.updateColumns(controller.getColumn(i_final));
                    columnDialog.getPanel(i_final).setColumn(columnIndex, false);
                }

                @Override
                public void columnRenamed(int columnIndex, String newName) {
                    newName = StringUtils.join(StringUtils.split(newName, " .,"), '_');
                    controller.setColumnName(i_final, columnIndex, newName);
                    TablePanel tp = (TablePanel) tabPane.getComponentAt(i_final);
                    tp.updateColumns(controller.getColumn(i_final));
                    columnDialog.getPanel(i_final).setColumnName(columnIndex, newName);
                }
            });

            table.setColumns(controller.getColumn(i));
        }
    }


    private void layoutComponents(int i) {
        if (i == 0) {
            layoutComponentsGridBag();
        } else if (i == 1) {
            layoutComponentsBorder();
        }
    }

    private void layoutComponentsBorder() {
        setLayout(new BorderLayout());
        add(toolbar, BorderLayout.PAGE_START);
//		add(logScrollPane, BorderLayout.WEST);
//		logScrollPane.setPreferredSize(new Dimension(200,400));
//		add(tabPane, BorderLayout.CENTER);
        add(splitPane, BorderLayout.CENTER);
    }

    private void layoutComponentsGridBag() {
        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();
		/*
		 * Initial Settings
		 */
        gc.gridy = 0;
        gc.gridwidth = 1;
        gc.weightx = 1;
        gc.weighty = 1;

		/*
		 * FIRST ROW
		 * 
		 * Toolbar across top
		 */
        gc.gridy++;
        gc.gridx = 0;
        gc.weighty = 0.1;
        gc.fill = GridBagConstraints.BOTH;
        add(toolbar, gc);

		/*
		 * SECOND ROW
		 * 
		 * On the left side, create a logging windows On the right, view of work
		 */
        gc.gridy += 1;

        gc.weighty = 5.0;
        gc.gridx = 0;
        gc.weightx = 0.1;
        // gc.anchor = GridBagConstraints.LINE_START;
        add(logScrollPane, gc);

        gc.weightx = 15.0;
        gc.gridx = 1;
        add(tabPane, gc);

    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        // ///////////////////////////////////////
        // File
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);


        // load regex processor
        JMenuItem loadProcessorMenu = new JMenuItem("Load Processor...");
        loadProcessorMenu.setMnemonic(KeyEvent.VK_P);
        fileMenu.add(loadProcessorMenu);

        loadProcessorMenu.addActionListener(e -> {
            regexProcFileChooser.revalidate();
            if (regexProcFileChooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION) {
                File procFile = regexProcFileChooser.getSelectedFile();
                controller.setup(procFile);
            }
            setupTableLayout();
            updateTable();
        });


        // regex file
        JMenuItem loadRegexMenu = new JMenuItem("Load Regex...");
        loadRegexMenu.setMnemonic(KeyEvent.VK_R);
        fileMenu.add(loadRegexMenu);

        loadRegexMenu.addActionListener(e -> {
            regexFileChooser.setMultiSelectionEnabled(true);
            regexFileChooserFrame.setVisible(true);
            regexFileChooser.revalidate();
            if (regexFileChooser.getExitValue() == JFileChooser.APPROVE_OPTION) {
                File[] regexFiles = regexFileChooser.getSelectedFiles();
                String delim = regexFileChooser.getDelimiter();
                boolean replaceExisting = regexFileChooser.isCheckBoxChecked();
                controller.updatePatterns(regexFiles, delim, replaceExisting);
            }
            updateTable();
        });

        // load data
        JMenu loadDataMenu = new JMenu("Load Data");
        loadDataMenu.setMnemonic(KeyEvent.VK_L);
        JMenuItem loadDataFromFile = new JMenuItem("From File...");
        loadDataFromFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
                InputEvent.CTRL_MASK));
        loadDataFromFile.setMnemonic(KeyEvent.VK_F);

        JMenuItem loadDataFromDB = new JMenuItem("From Table...");
        loadDataFromDB.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T,
                InputEvent.CTRL_MASK));
        loadDataFromDB.setMnemonic(KeyEvent.VK_T);

        loadDataMenu.add(loadDataFromFile);
        loadDataMenu.add(loadDataFromDB);
        fileMenu.add(loadDataMenu);

        loadDataFromFile.addActionListener(e -> {
            fileChooser.setMultiSelectionEnabled(true);
            if (fileChooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION) {
                controller.addTextFiles(Arrays.asList(fileChooser
                        .getSelectedFiles()));
            }
            updateTable();
        });

        loadDataFromDB.addActionListener(e -> {
            // database logic here
            databaseDialog.setConnectionConfigurations(
                    controller.getConnectionConfigurations("user"),
                    controller.getConnectionConfigurations("url"));
            databaseDialog.setVisible(true);
            updateTable();
        });

        // save data
        JMenu saveDataMenu = new JMenu("Export Data");
        saveDataMenu.setMnemonic(KeyEvent.VK_E);
        JMenuItem saveDataToFile = new JMenuItem("Save to File...");
        saveDataToFile.setMnemonic(KeyEvent.VK_S);
        saveDataToFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                InputEvent.CTRL_MASK));

        JMenuItem saveDataToDB = new JMenuItem("Create Table...");
        saveDataToDB.setMnemonic(KeyEvent.VK_T);
        saveDataToDB.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T,
                InputEvent.CTRL_MASK));

        saveDataMenu.add(saveDataToFile);
        saveDataMenu.add(saveDataToDB);
        fileMenu.add(saveDataMenu);

        saveDataToFile.addActionListener(e -> {
            saveFileChooserFrame.setVisible(true);
            saveFileChooser.revalidate();
            if (saveFileChooser.getExitValue() == JFileChooser.APPROVE_OPTION) {
                File outputFile = saveFileChooser.getSelectedFile();
                String delimiter = saveFileChooser.getDelimiter();
                int index = saveFileChooser.getTabIndex();
                logger.info("Selected tab to output is " + index);
                try {
                    controller.saveToFile(outputFile, delimiter, index);
                } catch (IOException ioe) {
                    logger.error("Error saving to file: " + ioe.toString());
                }
            }
        });

        saveDataToDB.addActionListener(e -> {
            writeDatabaseDialog.setConnectionConfigurations(
                    controller.getConnectionConfigurations("user"),
                    controller.getConnectionConfigurations("url"));
            writeDatabaseDialog.setVisible(true);
        });

        // close
        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.setMnemonic(KeyEvent.VK_X);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);

        exitItem.addActionListener(e -> {
            WindowListener[] listeners = getWindowListeners();
            for (WindowListener listener : listeners) {
                listener.windowClosing(null); // includes closing methods
            }
        });
        menuBar.add(fileMenu);

        // /////////////////////////////
        // Settings
        JMenu settingsMenu = new JMenu("Settings");
        settingsMenu.setMnemonic(KeyEvent.VK_S);

        JMenuItem columnMenuItem = new JMenuItem("Columns...");
        columnMenuItem.setMnemonic(KeyEvent.VK_C);
        columnMenuItem.addActionListener(e -> columnDialog.setVisible(true));

        JMenuItem prefsMenuItem = new JMenuItem("Preferences...");
        prefsMenuItem.setMnemonic(KeyEvent.VK_P);
        prefsMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,
                InputEvent.CTRL_MASK));
        prefsMenuItem.addActionListener(e -> prefsDialog.setVisible(true));

        JCheckBoxMenuItem hideLogMenuItem = new JCheckBoxMenuItem("Hide Logging Window");
        hideLogMenuItem.addActionListener(e -> {
            JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
            if (item.isSelected()) {
                splitPane.setDividerLocation(0.0);
            } else {
                splitPane.setDividerLocation(0.25);
            }
            logScrollPane.setVisible(!item.isSelected());
            repaint();
        });

        settingsMenu.add(columnMenuItem);
        settingsMenu.add(prefsMenuItem);
        settingsMenu.add(hideLogMenuItem);
        menuBar.add(settingsMenu);

        // ////////////////////////////////
        // Run
        JMenu runMenu = new JMenu("Run");
        runMenu.setMnemonic(KeyEvent.VK_R);
        JMenuItem processTextMenuItem = new JMenuItem("Process Text");
        processTextMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_R, InputEvent.CTRL_MASK));
        processTextMenuItem.setMnemonic(KeyEvent.VK_P);
        processTextMenuItem.addActionListener(e -> processText());

        // temporary location for testing of controller.runBatchMode
        JMenuItem runBatchMenu = new JMenuItem("Run Batch");
        runMenu.add(runBatchMenu);

        runBatchMenu.addActionListener(e -> outputColumnDialog.setVisible(true));

        JMenuItem clearTextMenuItem = new JMenuItem("Clear Data");
        clearTextMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_DELETE, InputEvent.CTRL_MASK));
        clearTextMenuItem.setMnemonic(KeyEvent.VK_C);
        clearTextMenuItem.addActionListener(e -> clearData());

        JMenuItem clearDocsMenuItem = new JMenuItem("Clear Documents");
        clearDocsMenuItem.setMnemonic(KeyEvent.VK_D);
        clearDocsMenuItem.addActionListener(e -> clearDocuments());
        runMenu.add(processTextMenuItem);
        runMenu.add(clearTextMenuItem);
        runMenu.add(clearDocsMenuItem);
        menuBar.add(runMenu);

        // ////////////////////////////////
        // Help
        JMenu helpMenu = new JMenu("Help");

        JMenuItem aboutMenuItem = new JMenuItem("About...");
        aboutMenuItem.addActionListener(e -> aboutPageDialog.setVisible(true));
        JMenuItem tutorialsMenuItem = new JMenuItem("Tutorials...");
        tutorialsMenuItem.addActionListener(e -> tutorialPageDialog.setVisible(true));

        helpMenu.add(aboutMenuItem);
        helpMenu.add(tutorialsMenuItem);
        menuBar.add(helpMenu);
        ///////////////////////////////////

        return menuBar;
    }

}
