/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * LogPanel.java    0.2 2013-10-18
 */
package org.ghri.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Logging area on the main GUI application
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 *
 */
public class LogPanel extends JPanel {

    /**
     * contains the logging text
     */
    private final JTextArea textArea;

    /**
     * Maintains and updates a text area with logging
     * information. The implementation does not explicitly
     * require logging.
     */
    public LogPanel() {
        textArea = new JTextArea();
        textArea.setLineWrap(true);   //splits on chars not words
        setLayout(new BorderLayout());
        add(textArea, BorderLayout.CENTER);

        setVisible(true);
    }

    /**
     * Add logging information to the contained text area
     *
     * @param text String to append to logger
     */
    public void append(String text) {
        textArea.append(text.trim() + '\n');
    }
}