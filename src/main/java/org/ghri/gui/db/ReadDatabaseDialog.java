/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * ReadDatabaseDialog.java 0.2 2013-10-18
 */
package org.ghri.gui.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ghri.gui.HintTextField;

import javax.swing.*;
import java.awt.*;

/**
 * GUI dialog for getting data from db table.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @see DatabaseDialogListener
 * @see DatabaseDialog
 * @since 0.2 2013-10-18
 */
public class ReadDatabaseDialog extends DatabaseDialog {

    private static final Logger logger = LogManager.getLogger(ReadDatabaseDialog.class);
    private final JButton loadBtn;
    private final JButton cancelBtn;
    private final JTextField tableName;
    private final JTextField nameColumn;
    private final HintTextField subnameColumn;
    private final HintTextField dateColumn;
    private final JTextField textColumn;
    private DatabaseDialogListener listener;

    public ReadDatabaseDialog(JFrame parent) {
        super(parent, "Load Data From Table", true);

        loadBtn = new JButton("Load");
        cancelBtn = new JButton("Cancel");

        tableName = new JTextField(10);
        nameColumn = new JTextField(10);
        subnameColumn = new HintTextField("*Optional*", 10);
        dateColumn = new HintTextField("*Optional*", 10);
        textColumn = new JTextField(10);


        layoutComponents();
        addListeners();
        setDimAndLocation(parent);
    }


    public void setListener(DatabaseDialogListener listener) {
        this.listener = listener;
    }


    private void addListeners() {
        /*
		 * Load button.
		 */
        loadBtn.addActionListener(e -> {
            String table = tableName.getText();
            String nameCol = nameColumn.getText();
            String subnameCol = subnameColumn.getText();
            if (subnameCol.equals(""))
                subnameCol = null;
            String dateCol = dateColumn.getText();
            if (dateCol.equals(""))
                dateCol = null;
            String textCol = textColumn.getText();

            logger.info("Reading Table: " + table);
            setVisible(false);

            if (listener != null) {
                listener.setPreferences(table, nameCol, subnameCol,
                        dateCol, textCol);
            }
        });
		
		/*
		 * Cancel button.
		 */
        cancelBtn.addActionListener(e -> {
            setVisible(false);
            resetFields();
        });
		
		/*
		 * Subname column: text disappears when clicked.
		 */
    }

    protected void setDimAndLocation(Component parent) {
        setSize(425, 400);
        setLocationRelativeTo(parent);
    }

    @Override
    public void setVisible(boolean b) {
        if (b)
            setDimAndLocation(getParent());
        super.setVisible(b);
    }

    private void resetFields() {
        tableName.setText("");
        nameColumn.setText("");
        subnameColumn.setText("");
        dateColumn.setText("");
        textColumn.setText("");
    }

    private void layoutComponents() {
        int space = 10;
        JPanel infoPanel = new JPanel();
        infoPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(space, space, space, space),
                BorderFactory.createTitledBorder("Connection Settings")
        ));
        infoPanel.setLayout(new GridBagLayout());
        layoutInfoPanel(infoPanel);

        JPanel tablePanel = new JPanel();
        tablePanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(space, space, space, space),
                BorderFactory.createTitledBorder("Table Settings")
        ));
        tablePanel.setLayout(new GridBagLayout());

        JPanel columnPanel = new JPanel();
        columnPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Column Names"),
                BorderFactory.createEmptyBorder(space, space, space, space)
        ));
        columnPanel.setLayout(new GridBagLayout());

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		/*
		 * Initial GridBagConstraints
		 * and commonly used variables
		 */
        GridBagConstraints gc = new GridBagConstraints();
        gc.gridx = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;
        Insets rightPadding = new Insets(0, 0, 0, 20);
        Insets leftPadding = new Insets(0, 45, 0, 0);
        Insets noPadding = new Insets(0, 0, 0, 0);
        Insets rightPlusPadding = new Insets(5, 5, 5, 20);
		
		/*
		 * Table Panel: Table Name
		 */
        gc.gridx = 0;
        gc.gridy++;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        tablePanel.add(new JLabel("Table:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        tablePanel.add(tableName, gc);
		
		/*
		 * Table Panel: Column Panel
		 */
        gc.gridx = 0;
        gc.gridwidth = 2;
        gc.gridheight = 10;
        gc.weighty = 10;
        gc.gridy++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = leftPadding;
        tablePanel.add(columnPanel, gc);
		
		/*
		 * Column Panel: Name Column
		 */
        gc.gridx = 0;
        gc.gridwidth = 1;
        gc.gridheight = 2;
        gc.weighty = 20;
        gc.gridy = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPlusPadding;
        columnPanel.add(new JLabel("Primary Id:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        columnPanel.add(nameColumn, gc);
		
		
		/*
		 * Column Panel: Subname Column
		 */
        gc.gridx = 0;
        gc.gridy += 5;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPlusPadding;
        columnPanel.add(new JLabel("Secondary Id:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        columnPanel.add(subnameColumn, gc);

		/*
		 * Column Panel: Date Column
		 */
        gc.gridx = 0;
        gc.gridy += 5;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPlusPadding;
        columnPanel.add(new JLabel("SQL Date:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        columnPanel.add(dateColumn, gc);
		
		/*
		 * Column Panel: Text Column
		 */
        gc.gridx = 0;
        gc.gridy += 5;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPlusPadding;
        columnPanel.add(new JLabel("Text:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        columnPanel.add(textColumn, gc);
		
		/*
		 * Buttons Panel
		 */
        Dimension btnSize = cancelBtn.getPreferredSize();
        loadBtn.setPreferredSize(btnSize);
        buttonsPanel.add(loadBtn);
        buttonsPanel.add(cancelBtn);
		
		/*
		 * Set panels
		 */

        setLayout(new BorderLayout());
        add(infoPanel, BorderLayout.NORTH);
        add(tablePanel, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
    }
}

