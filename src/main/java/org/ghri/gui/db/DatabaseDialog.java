/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DatabaseDialog.java 0.2 2013-10-18
 */
package org.ghri.gui.db;

import javax.swing.*;
import java.awt.*;

/**
 * Superclass for Read/Write DatabaseDialogs
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public abstract class DatabaseDialog extends JDialog {


    private final JLabel username;
    private final JLabel connectionURL;


    DatabaseDialog(JFrame parent, String title, boolean modal) {
        super(parent, title, modal);
        username = new JLabel("Not set.");
        username.setForeground(new Color(255, 0, 0));
        connectionURL = new JLabel("Not set.");
        connectionURL.setForeground(new Color(255, 0, 0));
    }


    protected abstract void setDimAndLocation(Component parent);

    @Override
    public void setVisible(boolean b) {
        if (b)
            setDimAndLocation(getParent());
        super.setVisible(b);
    }


    void layoutInfoPanel(JPanel infoPanel) {
        /*
		 * Initial GridBagConstraints
		 * and commonly used variables
		 */
        GridBagConstraints gc = new GridBagConstraints();
        gc.gridx = 0;
        gc.fill = GridBagConstraints.NONE;
        Insets rightPadding = new Insets(5, 5, 5, 20);
        Insets noPadding = new Insets(5, 5, 5, 5);
		
		/*
		 * User name
		 */
        gc.weightx = 0.01;
        gc.gridx = 0;
        gc.gridy++;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        infoPanel.add(new JLabel("Username:"), gc);

        gc.gridx += 1;
        gc.weightx = 1;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        infoPanel.add(username, gc);
		
		/*
		 * User name
		 */
        gc.gridx = 0;
        gc.gridy++;
        gc.weightx = 0.01;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        infoPanel.add(new JLabel("Connection URL:"), gc);

        gc.gridx += 1;
        gc.weightx = 1;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        infoPanel.add(connectionURL, gc);
    }


    public void setConnectionConfigurations(String user,
                                            String url) {
        setJLabelConfiguration(user, username);
        setJLabelConfiguration(url, connectionURL);
    }

    private void setJLabelConfiguration(String item, JLabel label) {
        if (item != null) {
            label.setText(item);
            label.setForeground(new Color(0, 0, 0));
        } else {
            label.setText("Not set.");
            label.setForeground(new Color(255, 0, 0));
        }
    }
}
