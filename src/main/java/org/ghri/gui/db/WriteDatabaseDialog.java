/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * WriteDatabaseDialog.java 0.2 2013-10-18
 */
package org.ghri.gui.db;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * GUI dialog for writing data to db table.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @see WriteDatabaseDialogListener
 * @see DatabaseDialog
 * @since 0.2 2013-10-18
 */
public class WriteDatabaseDialog extends DatabaseDialog {

    private final JButton exportBtn;
    private final JButton cancelBtn;

    private final JCheckBox createTableBtn;
    private final JTextField tableName;
    private final List<JRadioButton> buttonList;
    private final List<String> buttonNameList;

    private WriteDatabaseDialogListener listener;

    public WriteDatabaseDialog(JFrame parent) {
        super(parent, "Export Data To Table", true);

        exportBtn = new JButton("Export");
        cancelBtn = new JButton("Cancel");

        createTableBtn = new JCheckBox();
        createTableBtn.setSelected(true);
        tableName = new JTextField(10);
        buttonList = new ArrayList<>();
        buttonList.add(new JRadioButton());
        buttonList.add(new JRadioButton());
        buttonList.add(new JRadioButton());
        buttonList.add(new JRadioButton());

        buttonNameList = new ArrayList<>();
        buttonNameList.add("Documents");
        buttonNameList.add("Result Data");
        buttonNameList.add("Result Matches");
        buttonNameList.add("All Matches");

        ButtonGroup group = new ButtonGroup();
        buttonList.forEach(group::add);

        layoutComponents();
        addListeners();
        setDimAndLocation(parent);
    }

    private void layoutComponents() {
        int space = 10;
        /*
		 * buttons panel
		 */
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        Dimension btnSize = cancelBtn.getPreferredSize();
        exportBtn.setPreferredSize(btnSize);
        buttonsPanel.add(exportBtn);
        buttonsPanel.add(cancelBtn);
		
		/*
		 * info panel
		 */
        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new GridBagLayout());
        layoutInfoPanel(infoPanel);
        infoPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(space, space, space, space),
                BorderFactory.createTitledBorder("Connection Settings")
        ));
		/*
		 * tablePanel
		 */
        JPanel tablePanel = new JPanel();
        tablePanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(space, space, space, space),
                BorderFactory.createTitledBorder("Table Settings")
        ));
        tablePanel.setLayout(new GridBagLayout());
		
		/*
		 * TablePanel
		 * Initial GridBagConstraints
		 * and commonly used variables
		 */
        GridBagConstraints gc = new GridBagConstraints();
        gc.gridy = 0;
        gc.gridx = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.fill = GridBagConstraints.NONE;
        Insets rightPadding = new Insets(0, 0, 0, 20);
        Insets noPadding = new Insets(0, 0, 0, 0);
        Insets rightPlusPadding = new Insets(5, 5, 5, 20);
		
		/*
		 * Table Panel: Table Name
		 */
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;
        tablePanel.add(new JLabel("Destination Table:"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        tablePanel.add(tableName, gc);
		
		/*
		 * Table Panel: TabIndex RadioButtons
		 */
        gc.gridy += 1;
        gc.weighty = 0.01;
        for (int i = 0; i < buttonList.size(); i++) {
            gc.gridx = 0;
            gc.gridy += 1;
            gc.anchor = GridBagConstraints.EAST;
            gc.insets = rightPlusPadding;
            tablePanel.add(new JLabel(buttonNameList.get(i)), gc);

            gc.gridx++;
            gc.anchor = GridBagConstraints.WEST;
            gc.insets = noPadding;
            tablePanel.add(buttonList.get(i), gc);
        }
		
		/*
		 * TablePanel : Check Box
		 */
        gc.weighty = 1;
        gc.gridy += 2;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPlusPadding;
        tablePanel.add(new JLabel("Create Table"), gc);

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;
        tablePanel.add(createTableBtn, gc);
		
		/*
		 * set panels
		 */
        setLayout(new BorderLayout());
        add(infoPanel, BorderLayout.NORTH);
        add(tablePanel, BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.SOUTH);
    }

    private void addListeners() {
        exportBtn.addActionListener(e -> {
            String table = tableName.getText();
            int tabIndex = 0;
            for (int i = 0; i < buttonList.size(); i++) {
                if (buttonList.get(i).isSelected()) {
                    tabIndex = i;
                    break;
                }
            }
            boolean createTable = createTableBtn.isSelected();

            setVisible(false);

            if (listener != null) {
                listener.dialogCompleted(table, tabIndex, createTable);
            }
        });
		
		/*
		 * Cancel button.
		 */
        cancelBtn.addActionListener(e -> {
            setVisible(false);
            resetFields();
        });
    }

    private void resetFields() {
        tableName.setText("");
    }

    protected void setDimAndLocation(Component parent) {
        setSize(425, 400);
        setLocationRelativeTo(parent);
    }


    public void setListener(WriteDatabaseDialogListener listener) {
        this.listener = listener;
    }
}
