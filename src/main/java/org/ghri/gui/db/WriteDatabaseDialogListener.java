/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * WriteDatabaseDialogListener.java 0.2 2013-10-18
 */
package org.ghri.gui.db;

/**
 * Listens to WriteDatabaseDialog for updates to database
 * configurations.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @see WriteDatabaseDialog
 * @since 0.2 2013-10-18
 */
public interface WriteDatabaseDialogListener {

    void dialogCompleted(String tableName, int tabIndex, boolean createTable);

}
