/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * ColumnDialog.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

import org.ghri.gui.DialogLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Dialog showing which columns should be shown in the tabbed table
 * panel frame.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class ColumnDialog extends JDialog {

    private JTabbedPane columnTabPane;
    private List<OutputDataColumnDialog> outputDialogs;
    private File[] files;

    private ColumnListener listener;

    public ColumnDialog(Window parent, String title) {
        this(parent, title, null);
    }


    public ColumnDialog(Window parent, String title, File[] fileseeds) {
        super(parent, title, ModalityType.APPLICATION_MODAL);

        columnTabPane = new JTabbedPane();

        files = fileseeds;
        String[] tabs = new String[]{"Documents", "Result Data", "Result Matches", "All Matches"};
        if (includeOutputDialog()) {
            // init output dialogs
            outputDialogs = new ArrayList<>();

            // add first summary page
            OutputDataColumnDialog summaryDialog = new OutputDataColumnDialog(this, "Summary", -1, files[0], true);
            columnTabPane.addTab("Summary", null, summaryDialog, "Summary page.");

            outputDialogs.add(summaryDialog);
        }

        for (int i = 0; i < tabs.length; i++) {
            JComponent comp = new JPanel();
            if (i == 0)
                comp.add(new DataEntryColumnDialog(this, tabs[i], i));
            else
                comp.add(new ResultDataColumnDialog(this, tabs[i], i));
            if (includeOutputDialog() && files.length > i + 1) {
                OutputDataColumnDialog outDlg = new OutputDataColumnDialog(this, "Output", i, files[i + 1]);
                comp.add(outDlg);
                outputDialogs.add(outDlg);
            }
            columnTabPane.addTab(tabs[i], null, comp);
        }

        columnTabPane.setVisible(true);

        JButton okBtn;
        if (includeOutputDialog()) {
            okBtn = new JButton("Run");
        } else {
            okBtn = new JButton("OK");
        }
        okBtn.setMnemonic(KeyEvent.VK_C);
        okBtn.addActionListener(e -> {
            List<ColumnEvent> list = new ArrayList<>();
            addColumnPanelsToList(list, columnTabPane);
            listener.actionsPerformed(list);
            dispose();
        });

        JButton cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(e -> dispose());

        DialogLayout.layoutComponents(this, okBtn, cancelBtn, columnTabPane);
        setDimAndLocation(parent);
    }


    private void addColumnPanelsToList(List<ColumnEvent> list, Component c) {
        if (c instanceof ColumnPanel) {
            list.add(((ColumnPanel) c).getColumnEvent());
        } else if (c instanceof Container) {
            Container cont = (Container) c;
            if (cont.getComponents().length > 0) {
                for (Component comp : cont.getComponents()) {
                    addColumnPanelsToList(list, comp);
                }
            }
        }
    }


    private void setDimAndLocation(Component parent) {
        if (this.includeOutputDialog()) {
            setSize(492, 597);
        } else {
            setSize(472, 437);
        }
        setLocationRelativeTo(parent);
    }

    @Override
    public void setVisible(boolean b) {
        if (b)
            setDimAndLocation(getParent());
        super.setVisible(b);
    }

    public void setColumnListener(ColumnListener listener) {
        this.listener = listener;
    }

    public ColumnPanel getPanel(int index) {
        return (ColumnPanel) columnTabPane.getComponentAt(index);
    }


    public File[] getCurrentOutputDirectories() {
        if (includeOutputDialog()) {
            for (int i = 0; i < outputDialogs.size(); i++) {
                File file = outputDialogs.get(i).getCurrentDirectory();
                if (file != null) {
                    files[i] = file;
                }
            }
            return files;
        }
        return null;
    }


    private boolean includeOutputDialog() {
        return files != null && files.length > 0;
    }


}
