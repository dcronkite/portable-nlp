/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.gui.column;

import java.io.File;

/**
 * @author David
 * @version 0.1 2016-05-28
 */
public class OutputDataColumnEvent extends ColumnEvent {

    private final File[] files;
    private final String delimiter;

    public OutputDataColumnEvent(int tableIndex, String delimiter, File[] files) {
        super(tableIndex);

        this.delimiter = delimiter;
        this.files = files;
    }

    public File[] getFiles() {
        return files;
    }

    public File getFile() {
        if (files.length > 0) {
            return files[0];
        } else {
            return null;
        }
    }

    public String getDelimiter() {
        return delimiter;
    }
}
