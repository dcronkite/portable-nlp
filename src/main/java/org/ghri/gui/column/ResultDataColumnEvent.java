/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.gui.column;


public class ResultDataColumnEvent extends ResultMatchesColumnEvent {

    public ResultDataColumnEvent(int tableIndex, boolean id, boolean capture,
                                 boolean srcName, boolean srcSubname, boolean srcDate, boolean cat,
                                 boolean subcat, int context, boolean start, boolean end) {
        super(tableIndex, id, capture, srcName, srcSubname, srcDate, cat, subcat,
                context, start, end);
    }

}
