/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.gui.column;

import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author David
 * @version 0.1 2016-05-28
 */
public class OutputDataColumnDialog extends ColumnPanel {


    private final int tableIndex;
    private final JFileChooser fileChooser;
    private final JTextField chooseFileLabel;
    private final JTextField delTextField;
    private final JRadioButton tabBtn;
    private final JRadioButton delTextBtn;

    private final JButton chooseFileBtn;

    public OutputDataColumnDialog(Window parent, String title, final int tableIndex, File fileStartPath) {
        this(parent, title, tableIndex, fileStartPath, false);
    }

    public OutputDataColumnDialog(Window parent, String title, final int tableIndex, File fileStartPath, boolean multipleFiles) {

        this.tableIndex = tableIndex;

        fileChooser = new JFileChooser();
        if (fileStartPath != null) {
            fileChooser.setCurrentDirectory(fileStartPath);
        }

        chooseFileLabel = new JTextField(25);
        chooseFileLabel.setEditable(false);

        if (multipleFiles) {
            chooseFileBtn = new JButton("Input Files...");
        } else {
            chooseFileBtn = new JButton("Choose File...");
        }
        chooseFileBtn.addActionListener(e -> {
            if (multipleFiles) {
                fileChooser.setMultiSelectionEnabled(true);
                if (fileChooser.showSaveDialog(OutputDataColumnDialog.this) == JFileChooser.APPROVE_OPTION) {
                    List<String> fileNames = new ArrayList<>();
                    for (File file : fileChooser.getSelectedFiles()) {
                        fileNames.add(file.getAbsolutePath());
                    }
                    chooseFileLabel.setText(StringUtils.join(fileNames, ";"));
                }
            } else {
                if (fileChooser.showSaveDialog(OutputDataColumnDialog.this) == JFileChooser.APPROVE_OPTION) {
                    String fileName = fileChooser.getSelectedFile().getAbsolutePath();
                    chooseFileLabel.setText(fileName);
                }
            }
        });

        delTextField = new JTextField(5);
        delTextField.setText("||");
        delTextField.setEnabled(false);

        tabBtn = new JRadioButton("Tab", true);

        delTextBtn = new JRadioButton("", true);
        delTextBtn.addChangeListener(e -> {
            boolean isText = delTextBtn.isSelected();
            delTextField.setEnabled(isText);
            if (isText) {
                delTextField.setForeground(Color.BLACK);
            } else {
                delTextField.setForeground(Color.GRAY);
            }
        });

        ButtonGroup radioButtons = new ButtonGroup();
        radioButtons.add(tabBtn);
        radioButtons.add(delTextBtn);

        JPanel otherOption = new JPanel();
        otherOption.setLayout(new FlowLayout());
        otherOption.add(delTextBtn);
        otherOption.add(delTextField);

        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new FlowLayout());
        radioPanel.add(new JLabel(" Select Delimiter:"), FlowLayout.LEFT);
        radioPanel.add(tabBtn);
        radioPanel.add(otherOption);

        layoutComponents(radioPanel);
    }


    private void layoutComponents(JPanel radioPanel) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        int border = 20;
        panel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createTitledBorder("Output File"),
                        BorderFactory.createEmptyBorder(5, border, 73, border + 5)
                )
        );

        GridBagConstraints gc = new GridBagConstraints();

        gc.gridy = 0;

        gc.gridx = 0;
        gc.ipady = 2;
        gc.gridwidth = 2;
        gc.fill = GridBagConstraints.BOTH;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = new Insets(5, 5, 5, 5);


        gc.gridy += 1;
        JPanel chooseFilePanel = new JPanel();
        chooseFilePanel.setLayout(new FlowLayout());
        chooseFilePanel.add(chooseFileBtn, FlowLayout.LEFT);
        chooseFilePanel.add(chooseFileLabel);
        panel.add(chooseFilePanel, gc);


        gc.gridy += 1;
        panel.add(radioPanel, gc);

        add(panel, BorderLayout.CENTER);

    }

    @Override
    public ColumnEvent getColumnEvent() {
        String del;
        if (tabBtn.isSelected()) {
            del = "\t";
        } else {
            del = delTextField.getText();
        }

        if (fileChooser.getSelectedFiles().length > 0)
            return new OutputDataColumnEvent(tableIndex, del, fileChooser.getSelectedFiles());
        else
            return new OutputDataColumnEvent(tableIndex, del, new File[]{fileChooser.getSelectedFile()});
    }


    public File getCurrentDirectory() {
        return fileChooser.getCurrentDirectory();
    }
}
