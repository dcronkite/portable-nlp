/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DataEntryColumnEvent.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

/**
 * Contains info on how DataEntry (Document) columns were updated
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class DataEntryColumnEvent extends ColumnEvent {

    private final boolean name;
    private final boolean subname;
    private final boolean date;
    private final boolean textLength;
    private final boolean text;
    private final int textExcerptLength;


    public DataEntryColumnEvent(int tableIndex, boolean name, boolean subname,
                                boolean date, boolean textLength, boolean text,
                                int textExcerptLength) {
        super(tableIndex);
        this.name = name;
        this.subname = subname;
        this.date = date;
        this.textLength = textLength;
        this.text = text;
        this.textExcerptLength = textExcerptLength;
    }

    public boolean isName() {
        return name;
    }

    public boolean isSubname() {
        return subname;
    }

    public boolean isDate() {
        return date;
    }

    public boolean isTextLength() {
        return textLength;
    }

    public boolean isText() {
        return text;
    }

    public int getTextExcerptLength() {
        return textExcerptLength;
    }

}
