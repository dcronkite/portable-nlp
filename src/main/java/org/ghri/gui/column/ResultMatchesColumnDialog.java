/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * MatchDataColumnDialog.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Dialog for MatchData (Results) columns.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class ResultMatchesColumnDialog extends ColumnPanel {

    private final JCheckBox idBtn;
    private final JCheckBox dataCapturedBtn;
    private final JCheckBox regexCategoryBtn;
    private final JCheckBox regexSubCategoryBtn;
    private final JCheckBox srcNameBtn;
    private final JCheckBox srcSubnameBtn;
    private final JCheckBox srcDateBtn;
    private final JCheckBox contextTextBtn;
    private final JFormattedTextField contextText;
    private final JCheckBox startIndexBtn;
    private final JCheckBox endIndexBtn;
    private final JLabel contextLabel;


    private final int tableIndex;

    public ResultMatchesColumnDialog(Window parent, String title, final int tableIndex) {

        this.tableIndex = tableIndex;

        buttons = new ArrayList<>();

        idBtn = new JCheckBox("Unique ID");
        idBtn.setSelected(true);
        buttons.add(idBtn);

        regexCategoryBtn = new JCheckBox("Regex Category");
        regexCategoryBtn.setSelected(true);
        buttons.add(regexCategoryBtn);

        regexSubCategoryBtn = new JCheckBox("Regex Subcategory");
        regexSubCategoryBtn.setSelected(true);
        buttons.add(regexSubCategoryBtn);

        srcNameBtn = new JCheckBox("Source Primary Name");
        srcNameBtn.setSelected(true);
        buttons.add(srcNameBtn);

        srcSubnameBtn = new JCheckBox("Source Secondary Name");
        srcSubnameBtn.setSelected(true);
        buttons.add(srcSubnameBtn);

        srcDateBtn = new JCheckBox("Source Date");
        srcDateBtn.setSelected(true);
        buttons.add(srcDateBtn);

        startIndexBtn = new JCheckBox("Start Index");
        startIndexBtn.setSelected(true);
        buttons.add(startIndexBtn);

        endIndexBtn = new JCheckBox("End Index");
        endIndexBtn.setSelected(true);
        buttons.add(endIndexBtn);

        dataCapturedBtn = new JCheckBox("Captured Data");
        dataCapturedBtn.setSelected(true);
        buttons.add(dataCapturedBtn);

        contextTextBtn = new JCheckBox("Context of Matches");
        contextTextBtn.setSelected(false);
        buttons.add(contextTextBtn);

        contextText = new JFormattedTextField(5);
        contextText.setValue(20);
        contextText.setEnabled(false);

        contextLabel = new JLabel("Text offset:");
        contextLabel.setForeground(Color.GRAY);

        contextTextBtn.addActionListener(e -> {
            boolean isTicked = contextTextBtn.isSelected();
            contextText.setEnabled(isTicked);
            if (isTicked) {
                contextLabel.setForeground(Color.BLACK);
            } else {
                contextLabel.setForeground(Color.GRAY);
                contextText.setValue(20);
            }
        });
        buttons.add(contextTextBtn);

        layoutComponents();
    }

    private void layoutComponents() {
        JPanel checkboxPanel = new JPanel();
        checkboxPanel.setLayout(new GridBagLayout());
        int border = 15;
        checkboxPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Select Columns to Include"),
                BorderFactory.createEmptyBorder(5, border, 0, border)));

        GridBagConstraints gc = new GridBagConstraints();

        gc.gridy = 0;

        gc.gridx = 0;
        gc.ipady = 2;
        gc.gridwidth = 2;
        gc.fill = GridBagConstraints.BOTH;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = new Insets(5, 5, 5, 5);


        gc.gridy += 1;
        checkboxPanel.add(idBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(dataCapturedBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(regexCategoryBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(regexSubCategoryBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(srcNameBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(srcSubnameBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(srcDateBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(contextTextBtn, gc);

        gc.insets = new Insets(5, 30, 5, 5);
        gc.gridy += 1;
        gc.gridwidth = 1;
        checkboxPanel.add(contextLabel, gc);
        gc.insets = new Insets(5, 5, 5, 5);
        gc.gridx = 1;
        checkboxPanel.add(contextText, gc);

        gc.gridx = 0;
        gc.gridwidth = 2;
        gc.gridy += 1;
        checkboxPanel.add(startIndexBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(endIndexBtn, gc);

        add(checkboxPanel, BorderLayout.CENTER);
    }

    @Override
    public ColumnEvent getColumnEvent() {
        int context = -1;
        if (contextTextBtn.isSelected()) {
            context = (Integer) contextText.getValue();
        }
        return new ResultMatchesColumnEvent(tableIndex, idBtn.isSelected(),
                dataCapturedBtn.isSelected(), srcNameBtn.isSelected(),
                srcSubnameBtn.isSelected(), srcDateBtn.isSelected(),
                regexCategoryBtn.isSelected(),
                regexSubCategoryBtn.isSelected(), context,
                startIndexBtn.isSelected(), endIndexBtn.isSelected());
    }


}
