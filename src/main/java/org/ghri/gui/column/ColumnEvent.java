/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * ColumnEvent.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

/**
 * Relays information about a change in a particular tab.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public abstract class ColumnEvent {

    private final int tableIndex;

    ColumnEvent(int index) {
        this.tableIndex = index;
    }

    public int getTableIndex() {
        return tableIndex;
    }
}
