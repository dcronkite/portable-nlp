/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * ColumnPanel.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Superclass for ColumnDialogs
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public abstract class ColumnPanel extends JPanel {

    List<JCheckBox> buttons;

    public abstract ColumnEvent getColumnEvent();

    public void setColumn(int index, boolean b) {
        buttons.get(index).setSelected(b);
        for (ActionListener listener : buttons.get(index).getActionListeners()) {
            listener.actionPerformed(null);
        }
    }

    public void setColumnName(int columnIndex, String newName) {
        buttons.get(columnIndex).setName(newName);
    }
}
