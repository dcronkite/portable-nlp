/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * MatchDataColumnEvent.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

/**
 * Contains info on how MatchData (Result) columns were updated
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class ResultMatchesColumnEvent extends ColumnEvent {

    private final boolean id;
    private final boolean capture;
    private final boolean srcName;
    private final boolean srcSubname;
    private final boolean srcDate;
    private final boolean cat;
    private final boolean subcat;
    private final int context;
    private final boolean start;
    private final boolean end;

    public ResultMatchesColumnEvent(int tableIndex, boolean id, boolean capture,
                                    boolean srcName, boolean srcSubname, boolean srcDate, boolean cat,
                                    boolean subcat, int context, boolean start, boolean end) {
        super(tableIndex);
        this.id = id;
        this.capture = capture;
        this.srcName = srcName;
        this.srcSubname = srcSubname;
        this.srcDate = srcDate;
        this.cat = cat;
        this.subcat = subcat;
        this.context = context;
        this.start = start;
        this.end = end;
    }

    public boolean isId() {
        return id;
    }

    public boolean isCapture() {
        return capture;
    }

    public boolean isSrcName() {
        return srcName;
    }

    public boolean isSrcSubname() {
        return srcSubname;
    }

    public boolean isSrcDate() {
        return srcDate;
    }

    public boolean isCat() {
        return cat;
    }

    public boolean isSubcat() {
        return subcat;
    }

    public boolean isContext() {
        return context >= 0;
    }

    public int getContext() {
        return context;
    }

    public boolean isStart() {
        return start;
    }

    public boolean isEnd() {
        return end;
    }
}
