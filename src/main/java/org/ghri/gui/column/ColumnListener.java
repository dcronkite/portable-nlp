/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * ColumnListener.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

import java.util.List;

/**
 * Lists to columns in tablepanel
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public interface ColumnListener {

    void actionsPerformed(List<ColumnEvent> ce);
}
