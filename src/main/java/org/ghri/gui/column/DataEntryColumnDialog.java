/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DataEntryColumnDialog.java 0.2 2013-10-18
 */
package org.ghri.gui.column;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Dialog for DataEntry (Document) columns.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class DataEntryColumnDialog extends ColumnPanel {

    private final JCheckBox nameBtn;
    private final JCheckBox subnameBtn;
    private final JCheckBox dateBtn;
    private final JCheckBox textLengthBtn;
    private final JCheckBox textBtn;
    private final JFormattedTextField contextText;
    private final JLabel contextLabel;

    private final int tableIndex;

    DataEntryColumnDialog(Window parent, String title, final int tableIndex) {

        this.tableIndex = tableIndex;

        buttons = new ArrayList<>();

        nameBtn = new JCheckBox("Primary Name");
        nameBtn.setSelected(true);
        buttons.add(nameBtn);

        subnameBtn = new JCheckBox("Secondary Name");
        subnameBtn.setSelected(true);
        buttons.add(subnameBtn);

        dateBtn = new JCheckBox("Date");
        dateBtn.setSelected(true);
        buttons.add(dateBtn);

        textLengthBtn = new JCheckBox("Text Length");
        textLengthBtn.setSelected(true);
        buttons.add(textLengthBtn);

        textBtn = new JCheckBox("Preview of Text");
        textBtn.setSelected(false);
        buttons.add(textBtn);

        contextText = new JFormattedTextField(5);
        contextText.setValue(20);
        contextText.setEnabled(false);

        contextLabel = new JLabel("Length:");
        contextLabel.setForeground(Color.GRAY);

        textBtn.addActionListener(e -> {
            boolean isTicked = textBtn.isSelected();
            contextText.setEnabled(isTicked);
            if (isTicked) {
                contextLabel.setForeground(Color.BLACK);
            } else {
                contextLabel.setForeground(Color.GRAY);
                contextText.setValue(20);
            }
        });

        layoutComponents();
    }

    private void layoutComponents() {
        JPanel checkboxPanel = new JPanel();
        checkboxPanel.setLayout(new GridBagLayout());
        int border = 20;
        checkboxPanel.setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createTitledBorder("Select Columns to Include"),
                        BorderFactory.createEmptyBorder(5, border, 73, border + 5)));

        GridBagConstraints gc = new GridBagConstraints();

        gc.gridy = 0;

        gc.gridx = 0;
        gc.ipady = 2;
        gc.gridwidth = 2;
        gc.fill = GridBagConstraints.BOTH;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = new Insets(5, 5, 5, 5);


        gc.gridy += 1;
        checkboxPanel.add(nameBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(subnameBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(dateBtn, gc);

        gc.gridy = 1;
        gc.gridx = 4;
        checkboxPanel.add(textLengthBtn, gc);

        gc.gridy += 1;
        checkboxPanel.add(textBtn, gc);

        gc.insets = new Insets(5, 30, 5, 5);
        gc.gridy += 1;
        gc.gridwidth = 1;
        checkboxPanel.add(contextLabel, gc);
        gc.insets = new Insets(5, 5, 5, 5);
        gc.gridx = 5;
        checkboxPanel.add(contextText, gc);

        add(checkboxPanel, BorderLayout.CENTER);
    }

    @Override
    public ColumnEvent getColumnEvent() {
        int textExcerptLength = -1;
        if (textBtn.isSelected()) {
            textExcerptLength = (Integer) contextText.getValue();
        }
        return new DataEntryColumnEvent(tableIndex, nameBtn.isSelected(),
                subnameBtn.isSelected(), dateBtn.isSelected(),
                textLengthBtn.isSelected(), textBtn.isSelected(), textExcerptLength);
    }
}
