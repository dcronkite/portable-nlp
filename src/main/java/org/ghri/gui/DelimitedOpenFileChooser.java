/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * @author David
 * @version 0.1 2016-05-28
 */
class DelimitedOpenFileChooser extends JFileChooser {

    static Logger logger = LogManager.getLogger(DelimitedOpenFileChooser.class);

    private final JFileChooser fileChooser;

    private final JRadioButton textBtn;
    private final JTextField textField;
    private final JCheckBox checkbox;
    private final JRadioButton tabBtn;
    private final JFrame frame;
    private final ActionListener[] innerListeners;
    private JButton openBtn;
    private JButton cancelBtn;
    private String delimiter;
    private int exitValue;
    private DisposeListener disposeListener;


    DelimitedOpenFileChooser() {
        super();
        super.setName("Regex File Dialog");
        frame = new JFrame("Regex File Dialog");
        exitValue = JFileChooser.CANCEL_OPTION;
        fileChooser = new JFileChooser();
        fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        fileChooser.setMultiSelectionEnabled(true);
        setControlButtonsAreShown(false);
        frame.add(fileChooser);

        delimiter = "\t";

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        JPanel lowerPanel = new JPanel();
        lowerPanel.setLayout(new BorderLayout());

        ButtonGroup panelButtons = new ButtonGroup();

        textField = new JTextField(5);
        textField.setText("||");
        textField.setEnabled(false);

        checkbox = new JCheckBox("Replace Regular Expressions", false);

        tabBtn = new JRadioButton("Tab", true);

        JPanel tabPanel = new JPanel();
        tabPanel.add(tabBtn);
        tabPanel.setLayout(new FlowLayout());

        textBtn = new JRadioButton("", false);
        textBtn.addChangeListener(e -> {
            boolean isText = textBtn.isSelected();
            textField.setEnabled(isText);
            if (isText) {
                textField.setForeground(Color.BLACK);
            } else {
                textField.setForeground(Color.GRAY);
            }
        });

        // radio groups
        panelButtons.add(tabBtn);
        panelButtons.add(textBtn);

        JPanel otherOption = new JPanel();
        otherOption.setLayout(new FlowLayout());
        otherOption.add(textBtn);
        otherOption.add(textField);
        otherOption.add(checkbox);

        JPanel radioPanel = new JPanel();
        radioPanel.setLayout(new FlowLayout());
        radioPanel.add(new JLabel(" Select Delimiter:"), FlowLayout.LEFT);
        radioPanel.add(tabPanel);
        radioPanel.add(otherOption);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        openBtn = null;
        cancelBtn = null;
        setOpenAndCancelButtons(fileChooser);
        innerListeners = openBtn.getActionListeners();
        openBtn.addActionListener(e -> {
            for (ActionListener al : innerListeners) {
                al.actionPerformed(e);
            }

            if (tabBtn.isSelected()) {
                delimiter = "\t";
            } else if (textBtn.isSelected()) {
                delimiter = textField.getText();
            }
            setSelectedFiles(fileChooser.getSelectedFiles());
            if (getSelectedFiles() != null)
                exitValue = JFileChooser.APPROVE_OPTION;
            else
                exitValue = JFileChooser.CANCEL_OPTION;
            frame.dispose();
            disposeListener.dispose();
        });

        cancelBtn.addActionListener(e -> {
            setSelectedFile(null);
            exitValue = JFileChooser.CANCEL_OPTION;
            disposeListener.dispose();
        });

        buttonsPanel.add(openBtn);
        buttonsPanel.add(cancelBtn);

        panel.add(radioPanel, BorderLayout.WEST);
        panel.add(buttonsPanel, BorderLayout.EAST);

        lowerPanel.add(panel, BorderLayout.WEST);

        setLayout(new BorderLayout());
        add(fileChooser, BorderLayout.CENTER);
        add(lowerPanel, BorderLayout.SOUTH);
    }

    private void setOpenAndCancelButtons(Container cont) {
        for (int i = 0; i < cont.getComponentCount(); i++) {
            Component c = cont.getComponent(i);
            if (c instanceof JButton) {
                JButton btn = (JButton) c;

                if (btn.getText() != null) {
                    if (btn.getText().equals("Open")) {
                        openBtn = btn;
                    } else if (btn.getText().equals("Cancel"))
                        cancelBtn = btn;
                }
            } else if (c instanceof Container) {
                setOpenAndCancelButtons((Container) c);
            }
        }
    }

    String getDelimiter() {
        return delimiter;
    }

    void addDisposeListener(DisposeListener disposeListener) {
        this.disposeListener = disposeListener;
    }

    int getExitValue() {
        return exitValue;
    }

    boolean isCheckBoxChecked() {
        return checkbox.isSelected();
    }

    public File getCurrentDirectory() {
        if (fileChooser != null)
            return fileChooser.getCurrentDirectory();
        else
            return super.getCurrentDirectory();
    }

    public void setCurrentDirectory(File dir) {
        super.setCurrentDirectory(dir);
        if (fileChooser != null)
            fileChooser.setCurrentDirectory(dir);
    }
}
