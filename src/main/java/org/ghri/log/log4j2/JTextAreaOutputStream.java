/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * JTextAreaOutputStream.java 0.2 2013-10-18
 */
package org.ghri.log.log4j2;

import javax.swing.*;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Output stream to text area.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
class JTextAreaOutputStream extends OutputStream {

    /**
     * I think this text area is shared with LogPanel?
     */
    private final JTextArea textArea;

    public JTextAreaOutputStream(JTextArea textArea) {
        super();
        this.textArea = textArea;
    }

    @Override
    public void write(int i) throws IOException {
        textArea.append(String.valueOf((char) i));
    }

}
