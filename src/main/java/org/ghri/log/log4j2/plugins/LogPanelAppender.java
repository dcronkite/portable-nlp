/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * LogPanelAppender.java 0.2 2013-10-18
 * <p>
 * log4j2 plug-in
 */
package org.ghri.log.log4j2.plugins;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.ghri.gui.LogPanel;

import javax.swing.*;

/**
 * Plug-in for log4j2 allowing output to LogPanel in gui
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 *
 */
@Plugin(name = "LogPanel", category = "Core", elementType = "appender", printObject = true)
public final class LogPanelAppender extends AbstractAppender {

    private static LogPanel logPanel;

    private LogPanelAppender(String name, Layout<String> layout, Filter filter, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
        logPanel = null;
    }

    @PluginFactory
    public static LogPanelAppender createAppender(@PluginAttribute("name") String name,
                                                  @PluginAttribute("ignoreExceptions") String ignore,
                                                  @PluginElement("Layout") Layout<String> layout,
                                                  @PluginElement("Filters") Filter filter) {
        boolean ignoreExceptions = Boolean.parseBoolean(ignore);

        if (name == null) {
            LOGGER.error("No name provided for LogPanelAppender");
            return null;
        }

        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new LogPanelAppender(name, layout, filter, ignoreExceptions);
    }

    public static void setLogPanel(LogPanel panel) {
        logPanel = panel;
    }

    @Override
    public void append(LogEvent event) {
        final String message = event.getMessage().getFormattedMessage();

        if (logPanel != null) {
            SwingUtilities.invokeLater(() -> logPanel.append(message));
        }
    }

}
