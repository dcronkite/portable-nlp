/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DbInterface.java     0.2 2013-10-18
 */
package org.ghri.db;

/**
 * Basic interface for all database connections 
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18 *
 */
public interface DbInterface {

    /**
     * connect to database
     */
    void connect();

    /**
     * close connection
     */
    void close();
}
