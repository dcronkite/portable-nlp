/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DataType.java    0.2 2013-10-18
 */

package org.ghri.db;

import java.util.NoSuchElementException;

/**
 * Class that helps to convert Java types to 
 * a form acceptable by multiple database schemes.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class DataType {

    private static final String _VARCHAR = "VARCHAR";
    private static final String _DATETIME = "DATETIME";
    private static final String _INT = "INT";
    private static final String _TEXT = "TEXT";

    /**
     * Datatype using one of the static String fields
     */
    private final String value;

    /**
     * Equals 0 if datatype does not have a size component,
     * otherwise the required size.
     * -1 if max size (e.g., varchar(max))
     */
    private final int size;

    /**
     * Constructor for datatypes that have a relevant size
     * component, e.g., varchar
     *
     * @param value The relevant String representation of
     * the type (of the fields: e.g., "VARCHAR", "DATETIME", etc.)
     * @param size
     */
    private DataType(String value, int size) {
        this.value = value;
        this.size = size;
    }

    /**
     * Constructor for datatypes that don't require a size
     * component, e.g., datetime
     * Int is also included here so high integer values should
     * not be necessary.
     *
     * @param value the relevant String representation of the 'middle'
     * datatype between sql and java
     */
    private DataType(String value) {
        this(value, 0);
    }

    /**
     * Initialize varchar of a particular size
     *
     * @param size- size of varchar to be inserted
     * @return generated instance of DataType
     */
    public static DataType VARCHAR(int size) {
        if (size > 255)
            size = -1;
        return new DataType(_VARCHAR, size);
    }

    /**
     * Initialize Datatype of type datetime
     *
     * @return datetime instance of DataType
     */
    public static DataType DATETIME() {
        return new DataType(_DATETIME);
    }

    /**
     * Initialize Datatype of type int
     *
     * @return int instance of DataType
     */
    public static DataType INT() {
        return new DataType(_INT);
    }

    /**
     * Initialize Datatype of type text
     *
     * @return text instance of DataType
     */
    public static DataType TEXT() {
        return new DataType(_TEXT);
    }

    /**
     * If the current datatype has a size component.
     * Returns false if a call to {@link org.ghri.db.DataType#size()} would
     * throw an exception.
     *
     * @return true if size component
     */
    public boolean hasSize() {
        return size != 0;
    }

    /**
     * Returns size of the datatype
     *
     * @return datatype's size
     * @throw NoSuchElementException - no size component for current type
     */
    public Object size() {
        if (size == 0) {
            throw new NoSuchElementException("DataType has no size.");
        }
        if (size == -1) {
            return "MAX";
        } else {
            return size;
        }
    }

    /**
     * determine if value should be quoted in sql statement
     *
     * @return true- all values of this type should be quoted
     * in sql statement
     */
    public boolean isQuoted() {
        return value.equals(_VARCHAR) ||
                value.equals(_TEXT) ||
                value.equals(_DATETIME);
    }

    /**
     * String representation of value only
     * Does not show size... why? Not sure. TODO
     */
    public String toString() {
        return value;
    }

}
