/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * InputDataEntry.java 0.2 2013-10-18
 */

package org.ghri.emerge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;

/**
 * Represents an input document.
 * <p>
 * TODO: create a builder for InputDataEntry. Parameters are confusing.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class InputDataEntry implements DataItem {

    static Logger logger = LogManager.getLogger(InputDataEntry.class);

    private final String name;
    private final String subname;
    private final Date date;
    private final String text;

    private int offset;

    public InputDataEntry(String name, String subname, String text) {
        this(name, subname, null, text);
    }

    /**
     * Constructor.
     *
     * @param name-    primary id; required not null
     * @param subname- secondary id; nullable
     * @param date-    sql-styled data; nullable
     * @param text-    doc text; required not null
     */
    public InputDataEntry(String name, String subname, Date date, String text) {
        this.name = name;
        if (subname == null)
            this.subname = "";
        else
            this.subname = subname;
        this.date = date;
        this.text = text;
        this.offset = 20;
    }

    public InputDataEntry(String name, Date date, String text) {
        this(name, "", date, text);
    }

    public InputDataEntry(String name, String text) {
        this(name, "", text);
    }

    /**
     * get primary id
     *
     * @return primary id
     */
    public String getName() {
        return name;
    }

    /**
     * get secondary id
     *
     * @return secondary id
     */
    public String getSubname() {
        return subname;
    }

    /**
     * get date
     *
     * @return date
     */
    public Date getDate() {
        return date;
    }

    /**
     * first N characters in document where
     * N= offset
     *
     * @return String of text excerpt
     */
    private String getContext() {
        if (offset < getTextLength())
            return getText().substring(0, offset);
        else
            return getText();
    }

    /**
     * entire document text
     *
     * @return document text
     */
    public String getText() {
        return text != null ? text : "";
    }

    /**
     * length of document text
     *
     * @return text length
     */
    private int getTextLength() {
        return getText().length();
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;
    }


    @Override
    public Object get(int index) {
        switch (index) {
            case 0:
                return getName();
            case 1:
                return getSubname();
            case 2:
                return getDate();
            case 3:
                return getTextLength();
            case 4:
                return getContext();
        }
        return null;
    }

    public int size() {
        return 5;
    }
}
