/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.emerge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class ResultData implements DataItem {

    static Logger logger = LogManager.getLogger(ResultData.class);

    private final int id;
    private final List<String> dataCaptured;
    private final String sourceName;
    private final String sourceSubname;
    private final Date sourceDate;
    private final String regexCategory;
    private final String regexSubCategory;
    private final String text;
    private final int startIndex;
    private final int endIndex;
    private final int size; // size of only required elements
    private final List<String> elements; // results
    private int offset;

    private ResultData(MatchData md, int countCustomCategories, int offset) {
        this.id = md.getId();
        this.dataCaptured = md.getDataCaptured();
        this.sourceName = md.getSourceName();
        this.sourceSubname = md.getSourceSubname();
        this.sourceDate = md.getSourceDate();
        this.regexCategory = md.getRegexCategory();
        this.regexSubCategory = md.getRegexSubCategory();
        this.startIndex = md.getStartIndex();
        this.endIndex = md.getEndIndex();

        this.text = md.getText();

        this.elements = new ArrayList<>();
        this.offset = offset;
        this.size = 10;
    }

    public ResultData(MatchData md, int countCustomCategories) {
        this(md, countCustomCategories, 0);
    }

    @Override
    public String toString() {
        String s = "MatchResult [id=" + id + ", dataCaptured=" + dataCaptured
                + ", sourceName=" + sourceName + ", sourceSubname="
                + sourceSubname + ", sourceDate=" + sourceDate
                + ", regexCategory=" + regexCategory + ", regexSubCategory="
                + regexSubCategory + ", text=" + text + ", startIndex="
                + startIndex + ", endIndex=" + endIndex + ", offset=" + offset
                + ", elements=";
        for (String el : elements)
            s += "," + el;

        s += "]";
        return s;
    }

    public void addElement(String el) {
        elements.add(el);
    }

    private int getId() {
        return id;
    }

    private List<String> getDataCaptured() {
        return dataCaptured;
    }

    private String getSourceName() {
        return sourceName;
    }

    private String getSourceSubname() {
        return sourceSubname;
    }

    private Date getSourceDate() {
        return sourceDate;
    }

    private String getRegexCategory() {
        return regexCategory;
    }

    private String getRegexSubCategory() {
        return regexSubCategory;
    }

    /**
     * get N characters on both sides of captured text
     * where N= offset
     *
     * @return- text excerpt
     */
    private String getContextText() {
        int start = ((startIndex - offset < 0) ? 0 : startIndex - offset);
        int end = ((endIndex + offset >= text.length()) ? text.length() - 1 : endIndex + offset);
        return text.substring(start, end);
    }

    private int getStartIndex() {
        return startIndex;
    }

    private int getEndIndex() {
        return endIndex;
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;

    }

    @Override
    public Object get(int index) {
        if (index >= size())
            throw new IndexOutOfBoundsException(String.format("Index: %d, Size: %d", index, size));
        switch (index) {
            case 0:
                return getId();
            case 1:
                return getRegexCategory();
            case 2:
                return getRegexSubCategory();
            case 3:
                return getSourceName();
            case 4:
                return getSourceSubname();
            case 5:
                return getSourceDate();
            case 6:
                return getStartIndex();
            case 7:
                return getEndIndex();
            case 8:
            /*
		     * to avoid having multiple entries
		     * (possible regex mistake), just choose the first one
		     * TODO: not sure if this should be expected behavior
		     */
//			return getDataCaptured().toString();
                if (getDataCaptured().size() == 0) {
                    return null;
                }
                return getDataCaptured().get(0);
            case 9:
                return getContextText();
            default:
                return elements.get(index - size);
        }
    }


    @Override
    public int size() {
        return size + elements.size();
    }

}
