/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * MatchData.java 0.3 2013-10-18
 */
package org.ghri.emerge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.util.List;

/**
 * Contains regex matches from documents along with
 * certain metadata
 *
 * @author David Cronkite
 * @version 0.3 2013-10-18
 * @since 0.2 2013-10-18
 */
public class MatchData implements DataItem {

    static Logger logger = LogManager.getLogger(MatchData.class);

    private static int UNIQUE_ID = 0;

    private final int id;
    private final List<String> dataCaptured;
    private final String sourceName;
    private final String sourceSubname;
    private final Date sourceDate;
    private final String regexCategory;
    private final String regexSubCategory;
    private final String text;
    private final int startIndex;
    private final int endIndex;
    private int offset;

    /**
     * Constructor.
     *
     * @param dataCaptured
     * @param srcName
     * @param srcSubname
     * @param srcDate
     * @param regexCat
     * @param regexSubCat
     * @param start
     * @param end
     * @param text
     * @param offset
     */
    public MatchData(List<String> dataCaptured, String srcName,
                     String srcSubname, Date srcDate, String regexCat,
                     String regexSubCat, int start, int end, String text, int offset) {
        this.id = UNIQUE_ID;
        UNIQUE_ID++;

        this.dataCaptured = dataCaptured;
        this.sourceName = srcName;
        this.sourceSubname = srcSubname;
        this.sourceDate = srcDate;
        this.regexCategory = regexCat;
        this.regexSubCategory = regexSubCat;
        this.startIndex = start;
        this.endIndex = end;

        this.offset = offset;

        this.text = text;
    }


    @Override
    public String toString() {
        return "MatchData [id=" + id + ", dataCaptured=" + dataCaptured
                + ", sourceName=" + sourceName + ", sourceSubname="
                + sourceSubname + ", sourceDate=" + sourceDate
                + ", regexCategory=" + regexCategory + ", regexSubCategory="
                + regexSubCategory + ", text=" + text + ", startIndex="
                + startIndex + ", endIndex=" + endIndex + ", offset=" + offset
                + "]";
    }

    @Override
    public Object get(int index) {
        switch (index) {
            case 0:
                return getId();
            case 1:
                return getRegexCategory();
            case 2:
                return getRegexSubCategory();
            case 3:
                return getSourceName();
            case 4:
                return getSourceSubname();
            case 5:
                return getSourceDate();
            case 6:
                return getStartIndex();
            case 7:
                return getEndIndex();
            case 8:
            /*
             * to avoid having multiple entries
		     * (possible regex mistake), just choose the first one
		     * TODO: not sure if this should be expected behavior
		     */
                if (getDataCaptured().size() == 0) {
                    return null;
                }
                return getDataCaptured().get(0);
            case 9:
                return getContextText();
        }
        return null;
    }

    @Override
    public int size() {
        return 10;
    }

    /**
     * primary id
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * document primary id
     *
     * @return
     */
    public String getSourceName() {
        return sourceName;
    }

    /**
     * document secondary id
     *
     * @return
     */
    public String getSourceSubname() {
        return sourceSubname;
    }

    /**
     * document date
     *
     * @return
     */
    public Date getSourceDate() {
        return sourceDate;
    }

    /**
     * list of captured data
     *
     * @return
     */
    public List<String> getDataCaptured() {
        return dataCaptured;
    }

    /**
     * string of captured data
     *
     * @return
     */
    public String getCaptured(int i) {
        return dataCaptured.get(i);
    }

    /**
     * category of regex that made match
     *
     * @return
     */
    public String getRegexCategory() {
        return regexCategory;
    }

    /**
     * subcategory of regex that made match
     *
     * @return
     */
    public String getRegexSubCategory() {
        return regexSubCategory;
    }

    /**
     * get N characters on both sides of captured text
     * where N= offset
     *
     * @return- text excerpt
     */
    private String getContextText() {
        int start = ((startIndex - offset < 0) ? 0 : startIndex - offset);
        int end = ((endIndex + offset >= text.length()) ? text.length() - 1 : endIndex + offset);
        return text.substring(start, end);
    }

    public String getText() {
        return text;
    }

    /**
     * index where match starts in text
     *
     * @return
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * index+1 where match ends in text
     *
     * @return
     */
    public int getEndIndex() {
        return endIndex;
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;
    }


}
