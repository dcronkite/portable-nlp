/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DataItem.java    0.2 2013-10-18
 */
package org.ghri.emerge;

/**
 * Interface for DataItems like MatchData and DataEntry.
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public interface DataItem {

    /**
     * Offset size for text.
     * @param offset
     */
    void setOffset(int offset);

    /**
     * Each field must have a number associated with it.
     * @param index- retrieves a field from object
     * @return
     */
    Object get(int index);

    /**
     * Number of fields.
     * @return number of parameters
     */
    int size();
}
