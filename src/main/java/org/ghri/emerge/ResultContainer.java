/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.emerge;

import org.ghri.util.DataArrayList;

import java.util.List;

/**
 * This is a mutable tuple. It is only intended for short term use.
 * All lists may be mutable.
 * <p>
 * Created by David on 12/31/2014.
 */
class ResultContainer {

    private final DataArrayList<DataItem> nonRelevantMatches;
    private final DataArrayList<DataItem> categories;
    private final DataArrayList<DataItem> relevantMatches;

    public ResultContainer() {
        this.nonRelevantMatches = new DataArrayList<>();
        this.categories = new DataArrayList<>();
        this.relevantMatches = new DataArrayList<>();
    }

    public DataArrayList<DataItem> getNonRelevantMatches() {
        return nonRelevantMatches;
    }

    public void setNonRelevantMatches(List<MatchData> nonRelevantMatches) {
        this.nonRelevantMatches.addAll(nonRelevantMatches);
    }

    public DataArrayList<DataItem> getCategories() {
        return categories;
    }

    public void setCategories(List<ResultData> categories) {
        this.categories.addAll(categories);
    }


    public DataArrayList<DataItem> getRelevantMatches() {
        return relevantMatches;
    }

    public void setRelevantMatches(List<MatchData> relevantMatches) {
        this.relevantMatches.addAll(relevantMatches);
    }

    public DataArrayList<DataItem> getDataListByIndex(int index) {
        if (index == 0)
            throw new IndexOutOfBoundsException(String.format("Unrecognized column index: %d.", index));
        else if (index == 1)
            return getCategories();
        else if (index == 2)
            return getRelevantMatches();
        else if (index == 3)
            return getNonRelevantMatches();
        else
            throw new IndexOutOfBoundsException(String.format("Unrecognized column index: %d.", index));

    }
}
