/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * RegexProcessor.java 0.2 2013-10-18
 */
package org.ghri.emerge;

import org.ghri.regex.MatchCask;
import org.ghri.regex.PatternCask;

import java.util.List;

/**
 * Interface for Regular Expression algorithm.
 * <p>
 * Must be subclassed and algorithm included in
 * {@link org.ghri.emerge.RegexProcessor#match(String)}
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public interface RegexProcessor {

    void addPatterns(List<PatternCask> patterns);

    void removePatterns();

    /**
     * Regular expression algorithm. Returns matches.
     *
     * @param text- text of document to run algorithm against
     * @return matches
     */
    RegexResultsContainer<MatchCask> match(String text);


    /**
     * Allows user to debug regular expressions against a piece of text.
     * The expected implementation is that all regexes are run against
     * the text and added to the NonRelevantMatches portion of the container.
     *
     * @param text - text of document to run regexes against
     * @return regular expression matches
     * @since 0.5 2014-06-17
     */
    RegexResultsContainer<MatchCask> debug(String text);


    List<ResultData> categorize(List<MatchData> matches);


    @SuppressWarnings("SameReturnValue")
    int getCountCustomCategories();
}
