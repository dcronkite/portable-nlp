/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * RegexResultsContainer.java   0.2 2013-10-18
 */
package org.ghri.emerge;

import java.util.ArrayList;
import java.util.List;

/**
 * Container for T for regular expressions
 * and regular expression results.
 *
 * @param <T> either PatternCask or MatchCask
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class RegexResultsContainer<T> {

    /**
     * results of all matches
     */
    private final List<T> allMatches;

    /**
     * results of only those matches which are relevant
     * to the actual results
     */
    private final List<T> relevantMatches;

    /**
     * Container for matches.
     */
    public RegexResultsContainer() {
        allMatches = new ArrayList<>();
        relevantMatches = new ArrayList<>();
    }

    /**
     * @param mc- add match/pattern to relevant list
     */
    public void addRelevantMatch(T mc) {
        relevantMatches.add(mc);
        allMatches.add(mc);
    }

    /**
     * @param mc- add match/pattern to non relevant
     */
    public void addNonRelevantMatch(T mc) {
        allMatches.add(mc);
    }

    /**
     * @param mcs- add more matches/patterns to relevant list
     */
    public void addRelevantMatches(List<T> mcs) {
        relevantMatches.addAll(mcs);
        allMatches.addAll(mcs);
    }

    /**
     * @param mcs- add more matches/patterns to non relevant
     */
    public void addNonRelevantMatches(List<T> mcs) {
        allMatches.addAll(mcs);
    }

    /**
     * @return relevant matches
     */
    public List<T> getRelevantMatches() {
        return relevantMatches;
    }

    /**
     * @return non-relevant matches
     */
    public List<T> getNonRelevantMatches() {
        return allMatches;
    }

    /**
     * @return number of relevant matches
     */
    public int getRelevantMatchesSize() {
        return relevantMatches.size();
    }

    /**
     * @return count of non-relevant matches
     */
    public int getNonRelevantMatchSize() {
        return allMatches.size();
    }
}
