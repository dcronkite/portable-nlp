/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * Controller.java  0.2 2013-10-18
 */
package org.ghri.emerge;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ghri.db.DataType;
import org.ghri.emerge.db.*;
import org.ghri.gui.column.ColumnEvent;
import org.ghri.gui.column.DataEntryColumnEvent;
import org.ghri.gui.column.ResultDataColumnEvent;
import org.ghri.gui.column.ResultMatchesColumnEvent;
import org.ghri.regex.MatchCask;
import org.ghri.regex.PatternCask;
import org.ghri.util.DataArrayList;

import javax.swing.table.TableColumn;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Primary interface between gui, data elements, and algorithm.
 *
 * The constructor must be overridden in order to implement a new
 * algorithm.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class Controller {

    private final Logger logger = LogManager.getLogger();
    /**
     * result data from processing algorithm
     */
    private final DataArrayList<DataItem> resultMatches;
    /**
     * list of all matches
     */
    private final DataArrayList<DataItem> allMatches;
    /**
     * list of all results (following categorize())
     */
    private final DataArrayList<DataItem> resultData;
    /**
     * gui columns in the various tabs (e.g., Document, Results, etc.)
     */
    private final List<List<MetaColumn>> columns;
    private RegexProcessor processor;
    /**
     * documents/text to have algorithm process
     */
    private DataArrayList<DataItem> textList; // (name, text)
    /**
     * database connection
     */
    private EmergeDatabaseInterface edb;

    /**
     * Must be subclassed in order to implement. First call of subclass
     * must be to super() for init of variables
     *
     * Initializes data lists and columns
     */
    public Controller() {
        columns = new ArrayList<>();
        resultMatches = new DataArrayList<>();
        allMatches = new DataArrayList<>();
        resultData = new DataArrayList<>();
    }


    public boolean notReady() {
        return processor == null;
    }


    /**
     * Run one file at a time and output the results of that file. Use this if the size
     * of files is possibly greater than available memory.
     *
     * @param files
     * @param debug
     * @param dest
     * @param del
     * @param offset - TODO: Not sure if this is actually included further down
     */
    public void runBatchMode(List<File> files, boolean debug, File[] dest, String[] del, int offset) {
        if (notReady()) {
            logger.fatal("No regular expression processor specified.");
            return;
        }

        logger.info(String.format("File count: %d.", files.size()));
        // write headers
        writeHeaders(dest.length, dest, del);

        for (File f : files) {
            // read data
            if (f == null)
                continue;

            String text = getTextFile(f);
            if (text == null)
                continue;

            InputDataEntry ide = new InputDataEntry(f.getName(), text);

            // process data
            ResultContainer container = new ResultContainer();
            if (!processDatum(container, ide, debug, offset)) // is null
                continue;

            // output data instance
            outputDataEntry(dest.length, dest, del, ide, container);
        }
    }


    /**
     * Run one document at a time and output the results of that document. Use this if the size
     * of documents is possibly greater than available memory.
     *
     * @param table
     * @param name
     * @param subname
     * @param date
     * @param text
     * @param debug
     * @param dest
     * @param del
     * @param offset
     */
    public void runDbBatchMode(String table, String name, String subname, String date, String text, boolean debug,
                               File[] dest, String[] del, int offset) {
        if (notReady()) {
            logger.fatal("No regular expression processor specified.");
            return;
        }

        // write headers
        writeHeaders(dest.length, dest, del);

        if (edb.prepareStatement(table, name, subname, date, text)) {
            while (true) {
                InputDataEntry ide = edb.retrieveDatum();
                if (ide == null)
                    break;

                // process data
                ResultContainer container = new ResultContainer();
                if (!processDatum(container, ide, debug, offset)) // is null
                    continue;

                // output data instance
                outputDataEntry(dest.length, dest, del, ide, container);
            }
        }
    }


    /**
     * Output the relevant headers to files.
     *
     * @param tabCount
     * @param dest
     * @param del
     * @return
     */
    private void writeHeaders(int tabCount, File[] dest, String[] del) {
        StringBuilder sb;
        for (int i = 0; i < tabCount; i++) {
            if (dest[i] == null)
                continue;
            try {
                sb = columnHeadersToString(del[i], i);
                sb.append("\n");
                FileUtils.writeStringToFile(dest[i], sb.toString(), false);
            } catch (IOException e) {
                logger.error("Unable to write header to file.");
                e.printStackTrace();
                return;
            }
        }
    }


    /**
     * Output a single inputdataentry results to listed files.
     * Relies on ResultContainer for access to values.
     *
     * @param tabCount
     * @param dest
     * @param del
     * @param ide
     * @param container
     * @return
     */
    private void outputDataEntry(int tabCount, File[] dest, String[] del, InputDataEntry ide,
                                 ResultContainer container) {
        StringBuilder sb;
        for (int i = 0; i < tabCount; i++) {
            if (dest[i] == null)
                continue;

            if (i == 0) {
                DataArrayList<DataItem> list = new DataArrayList<>(1);
                list.add(ide);
                sb = columnsToString(list, del[i], "\n", i);
            } else
                sb = columnsToString(container.getDataListByIndex(i), del[i], "\n", i);

            try {
                FileUtils.writeStringToFile(dest[i], sb.toString(), true);
            } catch (IOException e) {
                logger.error("Unable to write to file.");
                e.printStackTrace();
                return;
            }
        }
    }


    /**
     * TODO: this is a piece of crap workaround since
     * this.processor must defined before these can
     * run, and this.processor is defined in a subclass.
     * Need to work on this...
     */
    private void loadColumnsRequiringProcessor() {
        columns.add(createDataEntryColumnEntry());
        columns.add(createResultDataColumnEntry());
        columns.add(createMatchDataColumnEntry());
        columns.add(createMatchDataColumnEntry());
    }


    private String getTextFile(File file) {
        FileInputStream fis = null;
        String text = null;
        try {
            fis = new FileInputStream(file);
            text = IOUtils.toString(fis);
        } catch (FileNotFoundException e) {
            logger.error("Error: File not found: " + e.toString());
        } catch (IOException e) {
            logger.error("Error reading file: " + e.toString());
        } finally {
            try {
                assert fis != null;
                fis.close();
            } catch (IOException e) {
                logger.error("Error: " + e.toString());
            }
        }
        return text;
    }


    /**
     * Add text from a list of files to the to-be-processed list
     *
     * @param files- list of files to be processed by algo
     */
    public void addTextFiles(List<File> files) {
        if (files.size() == 0) {
            logger.warn("No documents added.");
            return;
        }

        for (File f : files) {
            String text = getTextFile(f);
            if (text != null)
                textList.add(new InputDataEntry(f.getName(), text));
        }
        logger.info("Loaded Documents: " + files.size());
        logger.info("Total Documents: " + textList.size());
    }

    /**
     * Initializes edb for database interactions
     *
     * @param user- username for db
     * @param password- pswd for db; not required with integrated security (connURL)
     * @param connectionUrl- connection string
     */
    public void configureConnection(String user, String password,
                                    String connectionUrl) {
        if (edb != null)
            edb.close();
        String[] arr = StringUtils.split(connectionUrl, ":");
        if (arr.length == 1)
            return;
        logger.info("Attempting to connect to " + arr[1]);
        switch (arr[1]) {
            case "mysql":
                edb = new MysqlDb();
                break;
            case "sqlserver":
                edb = new SqlServerDb();
                break;
            case "postgresql":
                edb = new PostgresqlDb();
                break;
            case "odbc":
                edb = new AccessDb();
                break;
            case "ucanaccess":
                edb = new UCanAccessDb();  // also access
                break;
            default:
                logger.warn("Did not recognize database. Failed to connect.");
        }
        edb.configure(user, password, connectionUrl);
    }

    public boolean isConnected() {
        return edb != null && edb.isConnected();
    }

    /**
     * closes edb database connection
     */
    public void closeConnection() {
        if (edb != null)
            edb.close();
    }

    /**
     * Retrieves data from database connection. Fails silently,
     * though does provide logging information.
     *
     * @param tableName- name of datatable
     * @param nameCol- column with primary id
     * @param subnameCol- column with a secondary id (optional)
     * @param dateCol- column with date (optional)
     * @param textCol- column containing text
     * @throws SQLException
     */
    public void addTableEntries(String tableName, String nameCol,
                                String subnameCol, String dateCol, String textCol) throws SQLException {
        List<InputDataEntry> list;
        if (edb == null) {
            logger.error("No database connection.");
            return;
        } else {
            if (subnameCol != null)
                list = edb.retrieveData(tableName, nameCol, subnameCol, dateCol, textCol);
            else
                list = edb.retrieveData(tableName, nameCol, textCol);
            textList.addAll(list);
        }
        logger.info("New documents: " + list.size());
        logger.info("Total documents: " + textList.size());
    }

    /**
     * Process all data in textList regardless of whether or not it has been previously
     * processed.
     */
    public boolean processData() {
        return processData(false);  // default behavior is to not use debug mode
    }

    /**
     * Process all data in textList regardless of whether or not it has been previously
     * processed.
     */
    public boolean processData(boolean debug) {
        if (textList.size() == 0) {
            logger.warn("Unable to process documents: 0 documents found.");
            return false;
        }

        if (processor == null) {
            logger.error("Unable to process documents: no regular expressions loaded.");
        }

        int offset = 20;

        for (DataItem di : textList) {
            InputDataEntry ide = (InputDataEntry) di;

            ResultContainer container = new ResultContainer();
            if (processDatum(container, ide, debug, offset)) {
                resultMatches.addAll(container.getRelevantMatches());
                allMatches.addAll(container.getNonRelevantMatches());
                resultData.addAll(container.getCategories());
            }

        }

        return true;
    }


    private boolean processDatum(ResultContainer result, InputDataEntry ide, boolean debug, int offset) {
        RegexResultsContainer<MatchCask> container;
        if (debug)
            container = processor.debug(ide.getText());
        else
            container = processor.match(ide.getText());

        if (container == null)
            return false;

        List<MatchData> relevantMatches = convertToMatchData(container.getRelevantMatches(),
                ide.getName(), ide.getSubname(), ide.getDate(), ide.getText(), offset);
        result.setRelevantMatches(relevantMatches);

        result.setNonRelevantMatches(
                convertToMatchData(container.getNonRelevantMatches(),
                        ide.getName(), ide.getSubname(), ide.getDate(), ide.getText(), offset)
        );

        result.setCategories(processor.categorize(relevantMatches));

        return true;
    }


    /**
     * Add metadata to the captured instances.
     * Get the list
     *
     * @param list
     * @param srcName - primary id
     * @param srcSubname- optional secondary id
     * @param srcDate- optional date
     * @param text- entire text of report
     * @param offset
     * @return list of matches along with their metadata
     */
    private List<MatchData> convertToMatchData(List<MatchCask> list,
                                               String srcName, String srcSubname, Date srcDate, String text, int offset) {
        List<MatchData> data = new ArrayList<>();
        for (MatchCask m : list) {
            // convert all MatchCask to MatchData??
            MatchData mdata = new MatchData(m.getWantedCaptures(), srcName,
                    srcSubname, srcDate,
                    m.getCat(), m.getSubcat(), m.getWantedStart(),
                    m.getWantedEnd(), text, offset);
            data.add(mdata);
        }
        return data;
    }


    /**
     * All relevant results.
     *
     * @return- relevant matches with metadata
     */
    public DataArrayList<DataItem> getResultData() {
        return resultData;
    }

    /**
     * All relevant results.
     *
     * @return- relevant matches with metadata
     */
    public DataArrayList<DataItem> getResultMatches() {
        return resultMatches;
    }

    /**
     * All matches.
     *
     * @return- all matches with metadata
     */
    public DataArrayList<DataItem> getAllMatches() {
        return allMatches;
    }

    /**
     * All currently loaded documents
     *
     * @return- all currently loaded documents
     */
    public DataArrayList<DataItem> getDataEntries() {
        return textList;
    }

    /**
     * Get connection details wrt connection string and username
     * (not password).
     *
     * @param type- "url" or "user"
     * @return username or url; null if type not recognized
     */
    public String getConnectionConfigurations(String type) {
        if (edb != null) {
            if (type.equals("url")) {
                return edb.getConnectionURL();
            } else if (type.equals("user")) {
                return edb.getUser();
            }
        }
        return null;
    }

	/*
	 * 
	 * Columns
	 * 
	 * The following methods deal with maintenance, creation, deletion,
	 * of columns.
	 * 
	 */

    /**
     * Update all columns in tab "index" with information in
     * column event.
     *
     * @param ce- column event, either relevant to MatchData tabs (Matches)
     * or DataEntry tabs (Documents)
     * @param index- int of tab to be updated
     * @return resulting offset
     */
    public int updateColumn(ColumnEvent ce, int index) {
        int offset = 0;
        if (ce instanceof ResultMatchesColumnEvent) {
            ResultMatchesColumnEvent ev = (ResultMatchesColumnEvent) ce;
            columns.get(index).get(0).setVisible(ev.isId());
            columns.get(index).get(1).setVisible(ev.isCat());
            columns.get(index).get(2).setVisible(ev.isSubcat());
            columns.get(index).get(3).setVisible(ev.isSrcName());
            columns.get(index).get(4).setVisible(ev.isSrcSubname());
            columns.get(index).get(5).setVisible(ev.isSrcDate());
            columns.get(index).get(6).setVisible(ev.isStart());
            columns.get(index).get(7).setVisible(ev.isEnd());
            columns.get(index).get(8).setVisible(ev.isCapture());
            columns.get(index).get(9).setVisible(ev.isContext());
            if (ev.isContext()) {
                offset = ev.getContext();
            } else {
                offset = 0;
            }
        } else if (ce instanceof DataEntryColumnEvent) {
            DataEntryColumnEvent ev = (DataEntryColumnEvent) ce;
            columns.get(index).get(0).setVisible(ev.isName());
            columns.get(index).get(1).setVisible(ev.isSubname());
            columns.get(index).get(2).setVisible(ev.isDate());
            columns.get(index).get(3).setVisible(ev.isTextLength());
            columns.get(index).get(4).setVisible(ev.isText());
            if (ev.isText()) {
                offset = ev.getTextExcerptLength();
            } else {
                offset = 0;
            }
        } else //noinspection ConstantConditions
            if (ce instanceof ResultDataColumnEvent) {
                ResultDataColumnEvent ev = (ResultDataColumnEvent) ce;
                columns.get(index).get(0).setVisible(ev.isId());
                columns.get(index).get(1).setVisible(ev.isCat());
                columns.get(index).get(2).setVisible(ev.isSubcat());
                columns.get(index).get(3).setVisible(ev.isSrcName());
                columns.get(index).get(4).setVisible(ev.isSrcSubname());
                columns.get(index).get(5).setVisible(ev.isSrcDate());
                columns.get(index).get(6).setVisible(ev.isStart());
                columns.get(index).get(7).setVisible(ev.isEnd());
                columns.get(index).get(8).setVisible(ev.isCapture());
                columns.get(index).get(9).setVisible(ev.isContext());
                if (ev.isContext()) {
                    offset = ev.getContext();
                } else {
                    offset = 0;
                }
                // set custom categories to visible/invisible
			/*
			 * TODO this feature is not yet implemented in ColumnEvent
			for (int i=0; i<processor.getCountCustomCategories(); i++) {
				columns.get(index).get(i+9).setVisible( ev.isCustom(i));
			}
			*/
            }
        return offset;
    }

    /**
     * Get columns from tab "index"
     *
     * @param index- int of tab to get columns of
     * @return list of columns
     */
    public List<MetaColumn> getColumn(int index) {
        return columns.get(index);
    }

    /**
     * Set a column to be visible/not visible.
     *
     * @param index- int of tab
     * @param columnIndex- int of column index
     * @param b- true: visible; false: hidden
     */
    public void setColumnBoolean(int index, int columnIndex, boolean b) {
        columns.get(index).get(columnIndex).setVisible(b);
    }

    /**
     * Change column name.
     */
    public void setColumnName(int index, int columnIndex, String newName) {
        columns.get(index).get(columnIndex).setName(newName);
    }

    /**
     * Initialize columns for matchdata (matches).
     *
     * @return columns
     */
    private List<MetaColumn> createMatchDataColumnEntry() {
        List<MetaColumn> columns = new ArrayList<>();
        columns.add(new MetaColumn(true, "ID", new TableColumn(),
                DataType.INT()));
        columns.add(new MetaColumn(true, "Category", new TableColumn(),
                DataType.VARCHAR(50)));
        columns.add(new MetaColumn(true, "SubCategory", new TableColumn(),
                DataType.VARCHAR(50)));
        columns.add(new MetaColumn(true, "SourceName", new TableColumn(),
                DataType.VARCHAR(50)));
        columns.add(new MetaColumn(true, "SourceSubname", new TableColumn(),
                DataType.VARCHAR(50)));
        columns.add(new MetaColumn(true, "SourceDate", new TableColumn(),
                DataType.DATETIME()));
        columns.add(new MetaColumn(true, "StartIndex", new TableColumn(),
                DataType.INT()));
        columns.add(new MetaColumn(true, "EndIndex", new TableColumn(),
                DataType.INT()));
        columns.add(new MetaColumn(true, "CapturedData", new TableColumn(),
                DataType.VARCHAR(200)));
        columns.add(new MetaColumn(false, "Context", new TableColumn(),
                DataType.TEXT()));
        return columns;
    }

    /**
     * initialize columns for DataEntry (documents)
     *
     * @return columns
     */
    private List<MetaColumn> createDataEntryColumnEntry() {
        List<MetaColumn> columns = new ArrayList<>();
        columns.add(new MetaColumn(true, "Name",
                new TableColumn(), DataType.VARCHAR(50)));
        columns.add(new MetaColumn(true,
                "Subname", new TableColumn(), DataType.VARCHAR(50)));
        columns.add(new MetaColumn(true, "Date", new TableColumn(),
                DataType.DATETIME()));
        columns.add(new MetaColumn(true, "TextLength", new TableColumn(),
                DataType.INT()));
        columns.add(new MetaColumn(false, "Text", new TableColumn(),
                DataType.TEXT()));
        return columns;
    }


    /**
     * Initialize columns for matchresults (categorize).
     *
     * @return columns
     */
    private List<MetaColumn> createResultDataColumnEntry() {
        List<MetaColumn> columns = createMatchDataColumnEntry();
        if (processor != null) {
            for (int i = 0; i < processor.getCountCustomCategories(); i++) {
                columns.add(new MetaColumn(true, "Custom" + i, new TableColumn(),
                        DataType.VARCHAR(50)));
            }
        } else {
            logger.error("Columns may have loaded incorrectly: no regular expression processor found.");
        }
        return columns;
    }


    /**
     * Saves result column to output file using delimiter.
     * Only saves those columns which are currently set.
     *
     * @param outputFile- file to write to
     * @param delimiter
     * @param index- index of tab to output
     * @throws IOException
     */
    public void saveToFile(File outputFile, String delimiter, int index) throws IOException {
        StringBuilder sb = columnHeadersToString(delimiter, index);
        sb.append("\n");
        sb.append(columnsToString(getDataListByIndex(index), delimiter, "\n", index));

        FileUtils.writeStringToFile(outputFile, sb.toString());
    }

    /**
     * get string representation of columns using delimiter between columns
     *
     * @param delimiter
     * @param index- int of tab to use
     * @return
     */
    private StringBuilder columnHeadersToString(String delimiter, int index) {
        StringBuilder sb = new StringBuilder();
        // for all except first; fencepost column name
        columns.get(index).stream().filter(MetaColumn::isVisible).forEach(column -> {
            if (sb.length() > 0) // for all except first; fencepost
                sb.append(delimiter);
            sb.append(column.getName()); //column name
        });
        return sb;
    }

    /**
     * Get string representation of columns using delimiter between
     * columns and lineDelimit between rows.
     *
     * @param delimiter
     * @param lineDelimit
     * @param index- int of tab to use
     * @return
     */
    private StringBuilder columnsToString(DataArrayList<DataItem> list, String delimiter, String lineDelimit, int index) {
        StringBuilder sb = new StringBuilder();
        for (DataItem di : list) {
            boolean firstItem = true;
            for (int i = 0; i < di.size(); i++) {
                if (columns.get(index).get(i).isVisible()) {
                    if (!firstItem) {
                        sb.append(delimiter);
                    }
                    sb.append(removeLineBreaksFromString(String.valueOf(di.get(i))));
                    firstItem = false;
                }
            }
            sb.append(lineDelimit);
        }
        return sb;
    }

    /**
     *
     * @param s string to remove line endings, and replace these with a space
     * @return string without line endings
     */
    private String removeLineBreaksFromString(String s) {
        return s.replaceAll("\\r\\n|\\r|\\n", " ");
    }


    /**
     * Write results to db. If table already exists,
     * quietly moves on to trying to insert.
     *
     * Outputs failures to logging
     *
     * @param tableName- name of output table
     * @param index- tab to output
     * @param createTable- true: table should be created
     */
    public void writeToTable(String tableName, int index, boolean createTable) {
        List<String> columnNames = new ArrayList<>();
        List<DataType> columnTypes = new ArrayList<>();
        columnHeadersToList(index, columnNames, columnTypes);

        List<List<String>> rows = rowsToLists(index);

        if (createTable) {
            try {
                edb.createTable(tableName, columnNames, columnTypes);
            } catch (SQLException e) {
                logger.error("Error creating table: " + e.toString());
            }
        }
        try {
            edb.insertInto(tableName, columnNames, columnTypes, rows);
        } catch (SQLException e) {
            logger.error("Error inserting into table: " + e.toString());
        }

    }

    /**
     * Adds columns headers and column types to lists
     *
     * @param index- int of tab
     * @param columnNames- will be filled with names of each column
     * @param columnTypes- will be filled with types of each column
     */
    private void columnHeadersToList(int index, List<String> columnNames, List<DataType> columnTypes) {
        columns.get(index).stream().filter(MetaColumn::isVisible).forEach(column -> {
            columnNames.add(column.getName());
            columnTypes.add(column.getType());
        });
    }

    /**
     * Converts each row to a list, and puts each list of rows into
     * a list in preparation for inserting into table.
     *
     * @param index- int of tab to use.
     * @return
     */
    private List<List<String>> rowsToLists(int index) {
        List<List<String>> result = new ArrayList<>();
        for (DataItem di : getDataListByIndex(index)) {
            List<String> row = new ArrayList<>();
            logger.info(di.size());
            logger.info(columns.get(index).size());
            for (int i = 0; i < di.size(); i++) {
                if (columns.get(index).get(i).isVisible()) {
                    if (di.get(i) != null)
                        row.add(di.get(i).toString());
                    else
                        row.add(null);
                }
            }
            result.add(row);
        }
        return result;
    }


    /**
     * Get a DataList from an index.
     *
     * throws IndexOutOfBoundsEx: when index not in correct range
     * @param index
     * @return
     */
    private DataArrayList<DataItem> getDataListByIndex(int index) {
        logger.info(index);
        if (index == 0)
            return textList;
        else if (index == 1)
            return resultData;
        else if (index == 2)
            return resultMatches;
        else if (index == 3)
            return allMatches;
        else
            throw new IndexOutOfBoundsException(String.format("Unrecognized column index: %d.", index));
    }

    public void updatePatterns(InputStream regexFile, String delim, boolean replace) {
        List<PatternCask> patterns = PatternCask.loadInputStream(regexFile, delim);
        updatePatterns(patterns, replace);
    }

    public void updatePatterns(File[] regexFiles, String delim, boolean replace) {
        List<PatternCask> patterns = PatternCask.loadFiles(regexFiles, delim);
        updatePatterns(patterns, replace);
    }


    public void updatePatterns(List<PatternCask> patterns) {
        updatePatterns(patterns, false);
    }


    private void updatePatterns(List<PatternCask> patterns, boolean replace) {
        if (processor != null) {
            if (replace) {
                logger.warn("Removing existing patterns.");
                processor.removePatterns();
            }
            processor.addPatterns(patterns);
        } else {
            logger.error("Cannot add patterns: no regex processor loaded.");
        }
    }

    public void setup(File srcFile) {
        File clsFile;
        if (srcFile.getName().endsWith(".java")) {
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();  // requires jdk on classpath (not jre)

            compiler.run(null, null, null, srcFile.getPath());
            clsFile = new File(srcFile.getPath().replace(".java", ".class"));
        } else {
            clsFile = srcFile;
        }
        String className = clsFile.getName().substring(0, clsFile.getName().length() - 6);

        File dir;
        if (srcFile.exists()) {
            dir = srcFile.getParentFile();
        } else {
            logger.fatal(String.format("Specified java file does not exist: %s.", srcFile.getPath()));
            return;
        }

        URL dirURL;
        try {
            dirURL = dir.toURI().toURL();
        } catch (MalformedURLException e) {
            logger.fatal(String.format("Malformed url for class file at %s.", clsFile.getPath()));
            return;
        }

        ClassLoader cl = new URLClassLoader(new URL[]{dirURL});
        Class cls;
        try {
            cls = cl.loadClass(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        Object o = null;
        try {
            o = cls.getConstructors()[0].newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        processor = (RegexProcessor) o;
        textList = new DataArrayList<>();
        loadColumnsRequiringProcessor();
    }

}
