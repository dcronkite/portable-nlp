/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * MetaColumn.java 0.2 2013-10-18
 */
package org.ghri.emerge;

import org.ghri.db.DataType;

import javax.swing.table.TableColumn;

/**
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class MetaColumn {

    private String name;
    /**
     * true if column is visible in tab
     */
    private boolean visible;
    private TableColumn column;
    /**
     * type of data in column
     */
    private DataType type;

    /**
     * Constructor.
     *
     * @param visible
     * @param name
     * @param column
     * @param type
     */
    public MetaColumn(boolean visible, String name, TableColumn column, DataType type) {
        this.visible = visible;
        this.name = name;
        this.column = column;
        this.type = type;
    }


    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return true-column visible; false-column hidden
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible- true: column visible
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * @return
     */
    public TableColumn getColumn() {
        return column;
    }

    /**
     * @param column
     */
    public void setColumn(TableColumn column) {
        this.column = column;
    }

    /**
     * @return
     */
    public DataType getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(DataType type) {
        this.type = type;
    }
}
