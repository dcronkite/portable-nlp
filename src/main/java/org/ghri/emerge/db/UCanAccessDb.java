/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.emerge.db;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author crondj1
 * @version 0.1 2016-05-28
 */
public class UCanAccessDb extends EmergeDatabaseInterface {
    @Override
    public void configure() {

    }

    @Override
    public void connect() {
        if (conn != null)
            return;

        try {
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver").newInstance();
            logger.info("Obtained database driver for MS Access.");
        } catch (InstantiationException | ClassNotFoundException e) {
            logger.error("Database driver error: " + e.toString());
        } catch (IllegalAccessException e) {
            logger.error("Database access error: " + e.toString());
        }

        try {
            conn = DriverManager.getConnection(url, user, password);
            logger.info("Connected to MS Access database.");
        } catch (SQLException e) {
            logger.error("Database connection error: " + e.toString());
        }
    }

}
