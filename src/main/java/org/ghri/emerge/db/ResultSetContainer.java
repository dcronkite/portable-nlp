/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

package org.ghri.emerge.db;

import org.ghri.emerge.InputDataEntry;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Helper class to simplify database result iteration (i.e., ResultSets)
 *
 * @author David
 * @version 0.5 2016-05-10
 */
class ResultSetContainer {

    private final String name;
    private final String subname;
    private final String date;
    private final String text;
    private ResultSet results;
    private Statement stmt;


    ResultSetContainer(String name, String subname, String date, String text) {
        this.name = name;
        this.subname = subname;
        this.date = date;
        this.text = text;
    }

    void setSQL(Statement stmt, String sql) throws SQLException {
        this.stmt = stmt;
        results = stmt.executeQuery(sql);
    }

    boolean hasResults() {
        try {
            return results != null && !results.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    boolean hasSubname() {
        return subname != null;
    }

    boolean hasDate() {
        return date != null;
    }


    InputDataEntry getNextResult() throws SQLException {
        if (results.next()) {
            String valueName = determineType(results.getObject(name));
            String valueSubname = null;
            if (hasSubname())
                valueSubname = determineType(results.getObject(subname));
            Date valueDate = null;
            if (hasDate())
                try {
                    valueDate = results.getDate(date);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            String valueText = results.getString(text);
            InputDataEntry entry;
            if (hasSubname() && hasDate()) {
                entry = new InputDataEntry(valueName, valueSubname, valueDate, valueText);
            } else if (hasSubname()) {
                entry = new InputDataEntry(valueName, valueSubname, valueText);
            } else if (hasDate()) {
                entry = new InputDataEntry(valueName, valueDate, valueText);
            } else {
                entry = new InputDataEntry(valueName, valueText);
            }
            return entry;
        } else {
            results.close();
            stmt.close();
            return null;
        }
    }


    /**
     * Determine type of 'o' when retrieving data.
     *
     * @param o: expected Integer/String/Long, else null
     * @return type as String, null if not Integer/String/Long
     */
    private String determineType(Object o) {
        if (o instanceof Integer || o instanceof Long) {
            return o.toString();
        } else if (o instanceof String) {
            return (String) o;
        } else {
            return null;
        }
    }
}
