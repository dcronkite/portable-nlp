/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * EmergeDatabaseInterface.java     0.2 2013-10-18
 */
package org.ghri.emerge.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ghri.db.DataType;
import org.ghri.db.DbInterface;
import org.ghri.emerge.InputDataEntry;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Interface for accessing database. Subclasses
 * should implement specifics for different types of databases
 * (e.g., postgres, sql server, mysql, etc.)
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 *
 */
public abstract class EmergeDatabaseInterface implements DbInterface {

    final Logger logger = LogManager.getLogger(EmergeDatabaseInterface.class);

    Connection conn;
    String url;
    String user;
    String password;
    private ResultSetContainer results;

    /**
     * Attempt to configure connection using user, password, and connection string
     *
     * @param user- username for connection
     * @param password- password for connection (optional if used integrated security)
     * @param connectionUrl- connection string
     */
    public void configure(String user, String password, String connectionUrl) {
        this.user = user;
        this.password = password;
        this.url = connectionUrl;

        if (conn != null)
            close();
        connect();
    }

    /**
     * Whether or not there is an active connection.
     * @return connection is not null
     */
    public boolean isConnected() {
        return conn != null;
    }

    /**
     * Must be implemented by subclass. Should include connecting
     * to a particular Driver.
     */
    @SuppressWarnings("EmptyMethod")
    public abstract void configure();

    /**
     * get username
     *
     * @return user
     */
    public String getUser() {
        if (user != null)
            return user;
        return null;
    }

    /**
     * get connection string
     * @return connection string
     */
    public String getConnectionURL() {
        if (url != null)
            return url;
        return null;
    }

    /**
     * Close db connection.
     */
    @Override
    public void close() {
        try {
            if (conn != null)
                conn.close();
        } catch (SQLException e) {
            // TODO: do I need to do something here?
        }
        logger.info("Disconnected.");
    }

    /**
     * Create table.
     * This is not safe (no PreparedStatement)
     * and is liable to sql injection attack,
     * but the assumption is that this is the user's table...
     * so why would they want to attack it?
     *
     * @param tableName - tablename of document to be created
     * @param columns - list of column names
     * @param types - list of types for each column
     * @throws SQLException
     */
    public void createTable(String tableName, List<String> columns, List<DataType> types) throws SQLException {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(tableName).append("\n (");
        for (int i = 0; i < columns.size(); i++) {
            if (i > 0) {
                sql.append(",\n");
            }
            sql.append(columns.get(i)).append(" ").append(types.get(i).toString());
            if (types.get(i).hasSize())
                sql.append("(").append(types.get(i).size()).append(")");
        }
        sql.append("\n);");

        Statement stmt = conn.createStatement();

        logger.info("Create Table SQL Statement: " + sql.toString());
        stmt.execute(sql.toString());
    }

    /**
     * Inserts data into sql table.
     * This is not safe (no PreparedStatement)
     * and is liable to sql injection attack,
     * but the assumption is that this is the user's table...
     * so why would they want to attack it?
     *
     * @param tableName - already created table
     * @param columns - name of each column in table
     * @param types - types of each column in table
     * @param rows - list of rows, which are lists of column entries
     * @throws SQLException
     */
    public void insertInto(String tableName, List<String> columns, List<DataType> types,
                           List<List<String>> rows) throws SQLException {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(tableName).append("(");
        for (int i = 0; i < columns.size(); i++) {
            if (i > 0) {
                sql.append(", ");
            }
            sql.append(columns.get(i));
        }
        sql.append(")\n");
        System.out.println(sql.toString());
        Statement stmt = conn.createStatement();

        StringBuilder sql2;
        for (List<String> row : rows) {
            sql2 = new StringBuilder("VALUES (");
            for (int c = 0; c < types.size(); c++) {
                if (c > 0) {
                    sql2.append(", ");
                }
                String entry = row.get(c);
                if (entry == null || entry.length() == 0)
                    sql2.append("NULL");
                else if (types.get(c).isQuoted())
                    sql2.append("'").append(entry).append("'");
                else
                    sql2.append(entry);
            }
            sql2.append(");");
            sql2.insert(0, sql);
            logger.info("Insert into table statement: " + sql2.toString());
            stmt.executeUpdate(sql2.toString());
        }

    }

    /**
     * retrieve data from table table
     *
     * @param table- tablename
     * @param name- required, primary id
     * @param subname- can be null
     * @param date- can be null
     * @param text- required, text
     * @return list of documents with meta and text
     * @throws SQLException
     */
    public List<InputDataEntry> retrieveData(String table, String name, String subname, String date, String text) throws SQLException {
        /*
		 * The database belongs to the user, so no worries about sql injection.
		 */
        List<InputDataEntry> entries = new ArrayList<>();
        if (prepareStatement(table, name, subname, date, text)) {
            while (true) {
                InputDataEntry entry = results.getNextResult();
                if (entry != null)
                    entries.add(entry);
                else
                    break;
            }
        }
        logger.info("Documents read from source: " + entries.size());
        return entries;
    }

    public boolean prepareStatement(String table, String name, String subname, String date, String text) {
		/*
		 * The database belongs to the user, so no worries about sql injection.
		 */
        String sql;
        results = new ResultSetContainer(name, subname, date, text);
        if (results.hasSubname() && results.hasDate()) {
            sql = "SELECT " + name + "," + subname + "," + date + "," + text + " FROM " + table + ";";
        } else if (results.hasSubname()) {
            sql = "SELECT " + name + "," + subname + "," + text + " FROM " + table + ";";
        } else if (results.hasDate()) {
            sql = "SELECT " + name + "," + date + "," + text + " FROM " + table + ";";
        } else {
            sql = "SELECT " + name + "," + text + " FROM " + table + ";";
        }
        logger.debug(sql);
        try {
            results.setSQL(conn.createStatement(), sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        logger.debug("Results null: " + results.hasResults());
        return results.hasResults();
    }


    public InputDataEntry retrieveDatum() {
        if (results != null && results.hasResults()) {
            try {
                return results.getNextResult();
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error(e);
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * retrieve data from table table
     *
     * @param table- tablename
     * @param name- required, primary id
     * @param text- required, text
     * @return list of documents with meta and text
     * @throws SQLException
     */
    public List<InputDataEntry> retrieveData(String table, String name, String text) throws SQLException {
        return retrieveData(table, name, null, null, text);
    }
}
