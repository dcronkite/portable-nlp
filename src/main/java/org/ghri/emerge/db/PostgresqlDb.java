/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * PostgresqlDb.java 0.2 2013-10-18
 */
package org.ghri.emerge.db;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Database implementation with driver for Postgres Db
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class PostgresqlDb extends EmergeDatabaseInterface {

    @Override
    public void connect() {
        if (conn != null)
            return;

        try {
            Class.forName("org.postgresql.Driver").newInstance();
            logger.info("Obtained Postgres driver.");
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
            logger.error("Database driver error: " + e.toString());
        }
        try {
            conn = DriverManager.getConnection(url, user, password);
            logger.info("Connected to Postgresql.");
        } catch (SQLException e) {
            logger.error("Database connection error: Failed to connect: " + e.toString());
        }

    }

    @Override
    public void configure() {

    }

}
