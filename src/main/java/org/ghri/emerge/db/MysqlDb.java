/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * MysqlDb.java 0.2 2013-10-18
 */
package org.ghri.emerge.db;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Database implementation, loads driver for MySQL db.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @see EmergeDatabaseInterface
 * @since 0.2 2013-10-18
 */
public class MysqlDb extends EmergeDatabaseInterface {


    @Override
    public void connect() {
        if (conn != null)
            return;

//		url = "jdbc:mysql://localhost:3306/ghri-nlp";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            logger.info("Obtained database driver for MySQL.");
        } catch (InstantiationException | ClassNotFoundException e) {
            logger.error("Database driver error: " + e.toString());
        } catch (IllegalAccessException e) {
            logger.error("Database access error: " + e.toString());
        }

        try {
            conn = DriverManager.getConnection(url, user, password);
            logger.info("Connected to MySql database.");
        } catch (SQLException e) {
            logger.error("Database connection error: " + e.toString());
        }
    }


    @Override
    public void configure() {
    }


}
