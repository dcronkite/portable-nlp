/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * SqlServerDb.java 0.2 2013-10-18
 */
package org.ghri.emerge.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Database implementation for SqlServer Db
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class SqlServerDb extends EmergeDatabaseInterface {

    private final Logger logger = LogManager.getLogger(SqlServerDb.class);

    @Override
    public void connect() {
        if (conn != null)
            return;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            logger.info("SQL Server Driver found.");
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            logger.error(e);
        }

        try {
            conn = DriverManager.getConnection(url, user, password);
            logger.info("Connected to SqlServer.");
        } catch (SQLException e) {
            logger.error("Failed to connect: " + e.toString());
        }

    }

    @Override
    public void configure() {
    }
}
