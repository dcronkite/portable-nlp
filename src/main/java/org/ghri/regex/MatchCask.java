/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * MatchCask.java 0.2 2013-10-18
 */
package org.ghri.regex;

import com.google.code.regexp.MatchResult;
import com.google.code.regexp.Matcher;
import com.google.code.regexp.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * Container for Match objects.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class MatchCask implements MatchResult {

    private final Logger logger = LogManager.getLogger();
    //	private static int UNIQUE_ID = 0;
//	
//	private int id;
    private final String cat;
    private final String subcat;
    private final MatchResult matcher;

    private final String[] captures;

    private final Pattern integer = Pattern.compile("\\d+");
    private final int count;

    public MatchCask(String cat, String subcat, MatchResult matcher, String[] captures) {
        this(cat, subcat, matcher, captures, 0);
    }


    public MatchCask(String cat, String subcat, MatchResult matcher, String[] captures, int offset) {
//		this.id = UNIQUE_ID;
//		UNIQUE_ID ++;

        this.cat = cat;
        this.subcat = subcat;
        this.count = matcher.groupCount() + 1;
        this.matcher = matcher;
        this.captures = captures;

    }

    public String getCat() {
        return cat;
    }

    public String getSubcat() {
        return subcat;
    }

    public List<String> getWantedCaptures() {
        List<String> result = new ArrayList<>();
        if (this.captures != null) {
            for (String s : this.captures) {
                if (isInteger(s) && Integer.parseInt(s) < groupCount()) {
                    int i = Integer.parseInt(s);
                    result.add(this.matcher.group(i));
                } else if (matcher.namedGroups().keySet().contains(s)) {
                    result.add(this.matcher.group(s));
                } else {
                    logger.error("Unrecognized group %d for category %s, subcategory %s.", s, getCat(), getSubcat());
                }
            }
            return result;
        } else {
            return Collections.singletonList(this.matcher.group());
        }
    }

    public int getWantedEnd() {
        if (captures == null) {
            return this.end(0);
        }
        if (isInteger(captures[0])) {
            int i = Integer.parseInt(captures[0]);
            if (this.groupCount() > i) {
                return this.end(i);
            }
        }
        if (matcher.namedGroups().keySet().contains(captures[0])) {
            return this.end(captures[0]);
        }
        logger.warn("Invalid capture %s.", captures[0]);
        return this.end(0);
    }

    public int getWantedStart() {
        if (captures == null) {
            return this.start(0);
        }
        if (isInteger(captures[0])) {
            int i = Integer.parseInt(captures[0]);
            if (this.groupCount() > i) {
                return this.start(i);
            }
        }
        if (matcher.namedGroups().keySet().contains(captures[0])) {
            return this.start(captures[0]);
        }
        logger.warn("Invalid capture %s.", captures[0]);
        return this.start(0);
    }


    @Override
    public int start() {
        return this.start(0);
    }


    @Override
    public int start(int group) {
        try {
            return this.matcher.start(group);
        } catch (IndexOutOfBoundsException ex) {
            logger.fatal(
                    String.format("Unrecognized group index %d for category %s, subcategory %s.", group, getCat(), getSubcat()),
                    ex);
            throw ex;  // input error
        }
    }

    public int start(String group) {
        try {
            return this.matcher.start(group);
        } catch (IllegalArgumentException ex) {
            logger.fatal(
                    String.format("Unrecognized group name %s for category %s, subcategory %s.", group, getCat(), getSubcat()),
                    ex);
            throw ex;  // input error
        }
    }


    @Override
    public int end() {
        return this.end(0);
    }


    @Override
    public int end(int group) {
        try {
            return this.matcher.end(group);
        } catch (IndexOutOfBoundsException ex) {
            logger.fatal(
                    String.format("Unrecognized group index %d for category %s, subcategory %s.", group, getCat(), getSubcat()),
                    ex);
            throw ex;  // input error
        }
    }

    public int end(String group) {
        try {
            return this.matcher.end(group);
        } catch (IllegalArgumentException ex) {
            logger.fatal(
                    String.format("Unrecognized group name %s for category %s, subcategory %s.", group, getCat(), getSubcat()),
                    ex);
            throw ex;  // input error
        }
    }


    @Override
    public String group() {
        return this.group(0);
    }


    @Override
    public String group(int group) {
        try {
            return this.matcher.group(group);
        } catch (IndexOutOfBoundsException ex) {
            logger.fatal(
                    String.format("Unrecognized group index %d for category %s, subcategory %s.", group, getCat(), getSubcat()),
                    ex);
            throw ex;  // input error
        }
    }

    @Override
    public List<String> orderedGroups() {
        return null;
    }

    @Override
    public Map<String, String> namedGroups() {
        return null;
    }

    public String group(String group) {
        try {
            return this.matcher.group(group);
        } catch (IllegalArgumentException ex) {
            logger.fatal(
                    String.format("Unrecognized group name %s for category %s, subcategory %s.", group, getCat(), getSubcat()),
                    ex);
            throw ex;  // input error
        }
    }


    @Override
    public int groupCount() {
        return this.count;
    }


    private boolean isInteger(String s) {
        Matcher m = integer.matcher(s);
        return m.matches();
    }
}
