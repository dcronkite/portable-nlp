/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * PatternCask.java 0.2 2013-10-18
 */
package org.ghri.regex;

import com.google.code.regexp.Matcher;
import com.google.code.regexp.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Container for Regex Pattern objects.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class PatternCask {

    private static final Logger logger = LogManager.getLogger(PatternCask.class);

    private final String cat;
    private final String subcat;
    private final Pattern pattern;
    private final String[] captures;

    private PatternCask(String cat, String subcat, String text) {
        this(cat, subcat, text, null);
    }

    private PatternCask(String cat, String subcat, String text, String[] interestingGroups) {
        this(cat, subcat, text, interestingGroups, java.util.regex.Pattern.CASE_INSENSITIVE);
    }


    private PatternCask(String cat, String subcat, String text, String[] interestingGroups, int flags) {
        this.pattern = Pattern.compile(text.trim(), flags);
        this.cat = cat;
        this.subcat = subcat;
        this.captures = interestingGroups;
    }

    public static List<PatternCask> loadFiles(File[] files, String delim) {
        List<PatternCask> result = new ArrayList<>();
        for (File f : files) {
            result.addAll(loadFile(f, delim));
        }
        return result;
    }

    private static List<PatternCask> loadFile(File file, String delim) {
        List<PatternCask> list = new ArrayList<>();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));
            String text;
            while ((text = br.readLine()) != null) {
                PatternCask p = parseLine(text, delim);
                list.add(p);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                logger.catching(e);
            }
        }
        logger.info(String.format("Loaded %d regular expressions.", list.size()));
        return list;
    }

    public static List<PatternCask> loadInputStream(InputStream is, String delim) {
        List<PatternCask> list = new ArrayList<>();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(is));
            String text;
            while ((text = br.readLine()) != null) {
                PatternCask p = parseLine(text, "\\t");
                list.add(p);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ignored) {
            }
        }
        return list;
    }

    private static PatternCask parseLine(String line, String delim) {
        String[] results = line.split(delim);
        if (results.length == 3) {
            return new PatternCask(results[0], results[1], results[2]);
        } else if (results.length == 4) {  // specifies a desired capturing group
            return new PatternCask(results[0], results[1], results[2], StringUtils.split(results[3], ','));
        } else if (results.length >= 5) {
            // TODO: allow additions of flags (for regexes)
            // TODO: allow additions of notes
            return new PatternCask(results[0], results[1], results[2], StringUtils.split(results[3], ','));
        } else {
            return null;
        }
    }

    public String getCat() {
        return cat;
    }

    public String getSubcat() {
        return subcat;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public List<MatchCask> match(String text) {
        if (text != null) {
            return this.match(text, 0, text.length());
        } else {
            return new ArrayList<>();
        }
    }

    public List<MatchCask> match(String text, int start, int end) {
        List<MatchCask> matches = new ArrayList<>();
        Matcher matcher = pattern.matcher(text.substring(start, end));
        while (matcher.find()) {
            matches.add(new MatchCask(cat, subcat, matcher.toMatchResult(), captures, start));
        }
        return matches;
    }
}
