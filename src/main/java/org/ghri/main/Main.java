/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * Main.java 0.2 2013-10-18
 */
package org.ghri.main;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.plugins.util.PluginManager;
import org.ghri.emerge.Controller;
import org.ghri.gui.MainFrame;

import javax.swing.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class which initializes window
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);


    static {
        PluginManager.addPackage("org.ghri.log.log4j2");
    }

    static {
        String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = path;
        try {
            decodedPath = URLDecoder.decode(path, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e.toString());
        }

        File f = new File(decodedPath);

        try {
            System.load(Main.class.getResource("/dll/sqljdbc_auth.dll").getFile());
            logger.error("SQL JDBC Driver loaded");
        } catch (UnsatisfiedLinkError e) {
            logger.error(e.toString());
            try {
                System.loadLibrary("sqljdbc_auth.dll");
            } catch (UnsatisfiedLinkError e2) {
                logger.error(e2.toString());
            }
        }
    }

    public static void main(String[] args) {

        logger.info(System.getProperty("java.library.path"));

        // parse command line arguments, if available
        CommandMain cm = createOptions(args);
        Controller controller = new Controller();
        if (cm.processor != null) {
            controller.setup(new File(cm.processor));
        }
        if (cm.hasRegexFiles()) {
            controller.updatePatterns(cm.getRegexFiles(), cm.regexDel, true);
        }
        if (cm.hasDatabaseConnection()) {
            controller.configureConnection(cm.username, cm.password, cm.connectionString);
        }

        //noinspection ConstantConditions
        if (cm.silent) {
            int offset = 20;
            if (cm.hasTextFiles() || controller.isConnected()) {
                File[] dest = new File[]{
                        cm.doc.getOutFile(),
                        cm.result.getOutFile(),
                        cm.match.getOutFile(),
                        cm.debugc.getOutFile(),
                };
                String[] delimiter = {
                        cm.doc.getDelimiter(),
                        cm.result.getDelimiter(),
                        cm.match.getDelimiter(),
                        cm.debugc.getDelimiter(),
                };
                if (cm.hasTextFiles())
                    controller.runBatchMode(cm.getTextFiles(), cm.debug, dest, delimiter, offset);
                else
                    controller.runDbBatchMode(cm.db.tablename, cm.db.name, cm.db.subname, cm.db.date, cm.db.text,
                            cm.debug, dest, delimiter, offset);
            } else {
                logger.error("Unable to complete silent batch mode: Missing text files.");
            }
        } else {
            if (cm.hasTextFiles()) {
                controller.addTextFiles(cm.getTextFiles());
            }
            SwingUtilities.invokeLater(() -> new MainFrame(controller));

        }
    }

    private static CommandMain createOptions(String[] args) {

        CommandMain cm = new CommandMain();
        JCommander jc = new JCommander(cm);
        jc.addCommand("doc", cm.doc);
        jc.addCommand("result", cm.result);
        jc.addCommand("match", cm.match);
        jc.addCommand("debug", cm.debugc);
        jc.addCommand("db", cm.db);

        for (String arg : StringUtils.split(StringUtils.join(args, " "), ";")) {
            arg = arg.replaceAll("::", ";");
            String[] argv = StringUtils.split(arg);
            jc.parse(argv);
        }
        return cm;
    }
}

class CommandMain {

    private static final Logger logger = LogManager.getLogger(CommandMain.class);

    final CommandCommit doc;
    final CommandCommit result;
    final CommandCommit match;
    final CommandCommit debugc;
    final CommandDatabase db;
    @Parameter(names = "-debug", description = "Debug mode")
    boolean debug = false;
    @Parameter(names = "-silent", description = "Running silently, without opening the application.")
    boolean silent = false;
    @Parameter(names = "-processor", description = "Regular expression processor *.java or *.class file.")
    String processor;
    @Parameter(names = "-regexdelimiter", description = "Delimiter for regular expression files.")
    String regexDel = "\t";
    @Parameter(names = "-username", description = "Username for database connection")
    String username;
    @Parameter(names = "-password", description = "Username for database connection")
    String password;
    @Parameter(names = "-connectionstring", description = "Username for database connection")
    String connectionString;
    @Parameter(names = "-textfile", description = "Input files to process.")
    private List<String> textFiles;
    @Parameter(names = "-regexfile", description = "Input files to process.")
    private List<String> regexFiles;

    CommandMain() {
        this.doc = new CommandCommit();
        this.result = new CommandCommit();
        this.match = new CommandCommit();
        this.debugc = new CommandCommit();
        this.db = new CommandDatabase();
    }

    private List<File> getFiles(List<String> infiles) {
        List<File> files = new ArrayList<>();
        for (String s : infiles) {
            File f = new File(s);
            if (f.isDirectory()) {
                File[] dirFiles = f.listFiles();
                if (dirFiles != null) {
                    for (File file : dirFiles) {
                        if (file.isFile()) {
                            files.add(file);
                        } // ignore directories: only go one level deep
                    }
                }
            } else if (f.isFile()) {
                files.add(f);
            } else {
                logger.warn("Ignoring file: " + f.toString());
            }
        }
        return files;
    }

    List<File> getTextFiles() {
        return getFiles(textFiles);
    }

    File[] getRegexFiles() {
        List<File> files = getFiles(regexFiles);
        return files.toArray(new File[files.size()]);
    }

    boolean hasRegexFiles() {
        return regexFiles != null && regexFiles.size() > 0;
    }

    boolean hasTextFiles() {
        return textFiles != null && textFiles.size() > 0;
    }

    boolean hasDatabaseConnection() {
        return username != null && connectionString != null;
    }

}

@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
@Parameters(commandDescription = "Record changes to the repository")
class CommandCommit {

    @Parameter(description = "outFile <delimiter default=||>", required = true)
    private List<String> outFile;

    File getOutFile() {
        return new File(outFile.get(0));
    }

    String getDelimiter() {
        if (outFile.size() > 1) {
            return outFile.get(1);
        }
        return "||";  // default value
    }
}

@Parameters(commandDescription = "Database table options")
class CommandDatabase {

    @Parameter(names = "-tablename")
    String tablename;

    @Parameter(names = "-name")
    String name;

    @Parameter(names = "-subname")
    String subname;

    @Parameter(names = "-date")
    String date;

    @Parameter(names = "-text")
    String text;
}