/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * DataArrayList.java 0.2 2013-10-18
 */
package org.ghri.util;

import org.ghri.emerge.DataItem;

import java.util.ArrayList;

/**
 * ArrayList with an added field (offset) for keeping
 * track of an int value shared by all members of the
 * arraylist.
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.2 2013-10-18
 */
public class DataArrayList<T extends DataItem> extends ArrayList<T> {

    private int offset;

    public DataArrayList(int offset) {
        this.offset = offset;
    }

    public DataArrayList() {
        this(0);
    }

    public void updateOffset(int offset) {
        this.offset = offset;
        for (DataItem o : this) {
            o.setOffset(offset);
        }
    }

    @Override
    public boolean add(T t) {
        t.setOffset(offset);
        return super.add(t);
    }
}
