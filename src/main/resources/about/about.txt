           eMERGE Portable NLP v0.3

  The Portable NLP application seeks to provide an NLP solution which is
  both portable and easy to modify. This application is launched from a 
  distributable JAR file, and can be configured to run various
  regular expression-based algorithms.

  The goal of this application is allow easy distribution of NLP 
  algorithms. 

