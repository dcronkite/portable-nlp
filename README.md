# README #

Portable NLP is a regular expressions based application which extracts data from input text.


### Applying Portable NLP to a New Task ###

* Add regular expressions to the regex/regexes.txt file. The format is described below (see #Regex Format).
* Subclass org.ghri.emerge.Controller (e.g., org.ghri.emerge.caad.CAADController)
    * create constructor that loads relevant regex processor
+ Subclass org.ghri.emerge.RegexProcessor (e.g., org.ghri.emerge.caad.CAADRegexProcessor)
    * constructor: add each "category"/group of regexes to a list
    * match(text): organize how the categories will apply to any piece of text
    * categorize(matches): (optional implementation) simplify the captured results will additional regexes
* Modify org.ghri.MainFrame to instantiate the new Controller class
    * TODO: I don't think MainFrame should be modified like this...

#### Regex Format ####
 category | subcategory | regex | regex_capture 
---|---|---|---

* **category**: used by the application to determine how the regular expressions that belong to this group/"category" should behave
* **subcategory**: used for debugging only; make unique for each regex
* **regex**: regex to capture from text (use non-capturing (?: ... ) for all parenthesis sets that do not contain the desired value
* **regex_capture**: (warning! this might not be implemented/tested!) integer denoting which regex group should be treated as the result


### TODO ###
* Is there a way to dynamically load the desired algorithm from config files?


### Extending Portable NLP ###
Portable NLP is built with gradle using the Maven repository. If you plan to deploy to SQL Server, you may need to download the sqljdbc4 jar--just search for it.