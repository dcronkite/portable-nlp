/*
 * Copyright (c) 2016. Group Health Research Institute - All Rights Reserved
 * You may use, distribute, and modify this code according to the MIT license.
 * See LICENSE.txt or go to https://mit-license.org for full license details.
 */

/**
 * CAADRegexProcessor 0.3 2013-10-18
 */

import org.ghri.emerge.MatchData;
import org.ghri.emerge.RegexProcessor;
import org.ghri.emerge.RegexResultsContainer;
import org.ghri.emerge.ResultData;
import org.ghri.regex.MatchCask;
import org.ghri.regex.PatternCask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Regular Expression algorithm for CAAD
 *
 * @author David Cronkite
 * @version 0.2 2013-10-18
 * @since 0.3 2013-10-18
 */
public class CAADRegexProcessor implements RegexProcessor {

    private final List<PatternCask> impressionSectionStart;
    private final List<PatternCask> impressionSectionEnd;
    private final List<PatternCask> stenosisFound;
    private final List<PatternCask> stenosisPercent;
    private final List<PatternCask> stenosisAbsent;
    private final List<PatternCask> exam;
    private final List<PatternCask> ineligibleExam;

    /**
     * Constructor. Patterns will be added later.
     */
    public CAADRegexProcessor() {
        // initialize lists
        this.impressionSectionStart = new ArrayList<>();
        this.impressionSectionEnd = new ArrayList<>();
        this.stenosisFound = new ArrayList<>();
        this.stenosisPercent = new ArrayList<>();
        this.stenosisAbsent = new ArrayList<>();
        this.exam = new ArrayList<>();
        this.ineligibleExam = new ArrayList<>();
    }

    /**
     * Organize patterns into lists based on their category.
     * These lists will be used when implementing different
     * parts of the algorithm.
     *
     * @param patterns- list of patterns
     */
    public void addPatterns(List<PatternCask> patterns) {
        PatternCask p;
        for (PatternCask pattern : patterns) {
            p = pattern;
            if (p.getCat().equalsIgnoreCase("impression_begin")) {
                impressionSectionStart.add(p);
            } else if (p.getCat().equalsIgnoreCase("impression_end")) {
                impressionSectionEnd.add(p);
            } else if (p.getCat().equalsIgnoreCase("stenosis_found")) {
                stenosisFound.add(p);
            } else if (p.getCat().equalsIgnoreCase("stenosis_percent")) {
                stenosisPercent.add(p);
            } else if (p.getCat().equalsIgnoreCase("stenosis_absent")) {
                stenosisAbsent.add(p);
            } else if (p.getCat().equalsIgnoreCase("carotid_exam")) {
                exam.add(p);
            } else if (p.getCat().equalsIgnoreCase("ineligible_exam")) {
                ineligibleExam.add(p);
            }
        }
    }

    @Override
    public void removePatterns() {
        impressionSectionStart.clear();
        impressionSectionEnd.clear();
        stenosisFound.clear();
        stenosisPercent.clear();
        stenosisAbsent.clear();
        exam.clear();
        ineligibleExam.clear();
    }


    public RegexResultsContainer<MatchCask> match(String text) {
        RegexResultsContainer<MatchCask> stenosisMatches = new RegexResultsContainer<>();
        int impressionStart = -1;
        int impressionEnd = -1;

        // search for exam present
        boolean foundExam = false;
        for (PatternCask p : exam) {
            List<MatchCask> m = p.match(text);
            if (m.size() > 0) {
                foundExam = true;
                stenosisMatches.addNonRelevantMatches(m);
                break;
            }
        }

        for (PatternCask p : ineligibleExam) {
            List<MatchCask> m = p.match(text);
            if (m.size() > 0) {
                stenosisMatches.addNonRelevantMatches(m);
                return stenosisMatches;
            }
        }

        if (!foundExam) //only proceed if exam was found
            return stenosisMatches;

        // search for impression section start
        for (PatternCask p : impressionSectionStart) {
            List<MatchCask> m = p.match(text);
            if (m.size() > 0) {
                if (impressionStart == -1) { // only take first match
                    impressionStart = m.get(0).start();
                    impressionEnd = text.length();
                }
                //add all found matches for debugging purposes
                stenosisMatches.addNonRelevantMatches(m);
            }
        }

        // search for impression section end
        for (PatternCask p : impressionSectionEnd) {
            List<MatchCask> m = p.match(text);
            if (m.size() > 0) {
                // first time impression section end found, so update
                if (impressionEnd == text.length()) {
                    impressionEnd = m.get(0).start();
                }
                stenosisMatches.addNonRelevantMatches(m);
            }
        }

        // search impression section if it's found
        if (impressionEnd > impressionStart) {
            for (PatternCask p : stenosisPercent) {
                List<MatchCask> m = p.match(text, impressionStart, impressionEnd);
                if (m.size() > 0) {
                    stenosisMatches.addRelevantMatches(m);
                }
            }

            for (PatternCask p : stenosisAbsent) {
                List<MatchCask> m = p.match(text, impressionStart, impressionEnd);
                if (m.size() > 0) {
                    stenosisMatches.addRelevantMatches(m);
                }
            }

            for (PatternCask p : stenosisFound) {
                List<MatchCask> m = p.match(text, impressionStart, impressionEnd);
                if (m.size() > 0) {
                    stenosisMatches.addRelevantMatches(m);
                }
            }
        }


		/*
         * If there haven't been any stenosis measurements yet, search the non-impression section.
		 */
        if (stenosisMatches.getRelevantMatchesSize() == 0) {
            for (PatternCask p : stenosisPercent) {
                List<MatchCask> m = p.match(text);
                if (m.size() > 0) {
                    stenosisMatches.addRelevantMatches(m);
                }
            }

            for (PatternCask p : stenosisAbsent) {
                List<MatchCask> m = p.match(text);
                if (m.size() > 0) {
                    stenosisMatches.addRelevantMatches(m);
                }
            }

            for (PatternCask p : stenosisFound) {
                List<MatchCask> m = p.match(text);
                if (m.size() > 0) {
                    stenosisMatches.addRelevantMatches(m);
                }
            }
        }
        return stenosisMatches;
    }

    @Override
    public RegexResultsContainer<MatchCask> debug(String text) {
        RegexResultsContainer<MatchCask> stenosisMatches = new RegexResultsContainer<>();

        for (List<PatternCask> lst : Arrays.asList(
                impressionSectionStart,
                impressionSectionEnd,
                stenosisFound,
                stenosisPercent,
                stenosisAbsent,
                exam,
                ineligibleExam
        )) {
            for (PatternCask p : lst) {
                List<MatchCask> m = p.match(text);
                if (m.size() > 0) {
                    stenosisMatches.addNonRelevantMatches(m);
                }
            }
        }


        return stenosisMatches;
    }


    @Override
    public List<ResultData> categorize(List<MatchData> matches) {
        List<ResultData> results = new ArrayList<>();
        Pattern greaterPattern = Pattern.compile("(greater|more) than");
        Pattern lesserPattern = Pattern.compile("less than");
        Pattern digitPattern = Pattern.compile("\\d{1,3}");
        Pattern rangePattern = Pattern.compile("(\\d{1,3})\\s*-\\s*(\\d{1,3})");
        for (MatchData md : matches) {
			/*
			 * Convert absent and qualitative findings to quantitative values
			 */
            ResultData mr = new ResultData(md, getCountCustomCategories());

            // absent stenosis findings
            switch (md.getRegexCategory()) {
                case "stenosis_absent":
                    mr.addElement("0");
                    mr.addElement("0");

                    // qualitative stenosis findings
                    break;
                case "stenosis_found":
                    switch (md.getRegexSubCategory()) {
                        case "mod_carotid_sten":
                        case "mod_sten_carotid":
                            mr.addElement("50");
                            mr.addElement("79");
                            break;
                        case "severe_carotid_sten":
                        case "severe_sten_carotid":
                            mr.addElement("80");
                            mr.addElement("99");
                            break;
                        case "occl_carotid":
                        case "carotid_occl":
                            mr.addElement("100");
                            mr.addElement("100");
                            break;
                    }

                    // percentage stenosis findings
                    break;
                default:  // contains percentage
                    Matcher match = rangePattern.matcher(md.getCaptured(0));
                    if (match.find()) {
                        mr.addElement(match.group(1));
                        mr.addElement(match.group(2));
                    } else {
                        match = digitPattern.matcher(md.getCaptured(0));
                        if (match.find()) {
                            // subtract 1 from found value if preceded by "less than"
                            if (lesserPattern.matcher(md.getCaptured(0)).find()) {
                                String res = Integer.toString(Integer.parseInt(match.group()) - 1);
                                mr.addElement(res);
                                mr.addElement(res);
                                // add 1 to found value if preceded by "more|greater than"
                            } else if (greaterPattern.matcher((md.getCaptured(0))).find()) {
                                String res = Integer.toString(Integer.parseInt(match.group()) + 1);
                                mr.addElement(res);
                                mr.addElement(res);
                            } else {
                                mr.addElement(match.group());
                                mr.addElement(match.group());
                            }
                        } else {
                            mr.addElement("unknown");
                            mr.addElement("unknown");
                        }
                    }
                    break;
            }
            results.add(mr);
        }
        return results;
    }

    @Override
    public int getCountCustomCategories() {
        return 2; // two custom categories
    }
}
